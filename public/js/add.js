var form = document.querySelector(".searchform");
var resultHolder = document.querySelector("#results");
var authorField = document.querySelector("#authorField");
var authorLabel = document.querySelector("#authorLabel");
var titleField = document.querySelector("#titleField");
var titleLabel = document.querySelector("#titleLabel");
var isbnField = document.querySelector("#isbn");
var imageField = document.querySelector("#imageField");
var submitpick = document.querySelector("#submitpick");
var host = window.location.hostname;
var protocol = window.location.protocol;
if (host === "localhost") {
	host = host + ":3000";
}
var isRunning = false;

// Setup for add manually
var bookname = document.querySelector("#bookname-container");
var file = document.querySelector("#file-container");
// var isbn = document.querySelector("#isbn");
// var isbnHid = document.querySelector(".isbn-hid");
// var isbnhint = document.querySelector(".isbn-hint");
var author = document.querySelector("#authorField");
var title = document.querySelector("#titleField");
var manualImage = document.querySelector("input[name=manualPic]");


// Default picture placeholder
var noImage = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ8NDQ0NFREWFhURFRUZHSggGBolGxUVITEhJSkrLi8uFx8zODMtNygtLjcBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAaAAADAQEBAQAAAAAAAAAAAAABAgMABQQG/8QANBAAAwACAQIEBAUCBQUAAAAAAAECAxESBCEFEzFRQWFxoSIyQoGRcrEUM1LR8SRTgoOS/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/APrEhkjJDJAZIZIyQyQGSGSCkMkAEgpDJDJAKkHQ6QUgE0HQ+g8QE4m4lOJuIE+JuJTibQEuJtFeIOIEtCtFWgNASaFaKtCtASaFaKtCtASaFaKtCtASaFaKNCtAJoIdGAdIZIyQyQBSGSMkOkBkhkgpDJABIZIZIZIBUhkhkhkgE0HiUUh0BPiHiU0bQE+IOJXRtAS4g0V0BoCLQGirQrQEmhWirQrQEWhWizQjQEmhGirQrQEmhGirQjQCaMNowDpDpASHSAKQ6QEh0gMkOkZIdIDJDpGSHSACQyQyQyQCpB4jqQ6ATibiU0HQEuJuJXQNAS4gaK6A0BFoVos0K0BFoVos0I0BFoRos0I0BFoRos0I0BFoRoq0I0AmjB0YCiQ6QsookAUiiQsookAUh0jSh0gMkOkZIokAEh0jJDpAKkMkMkMkAnEOh9B0BPQOJXQNASaA0VaA0BFoVoq0K0BFoRos0I0BFoRos0TpARaEaLNE6QEmibRakTpAT0YbRgHlFJQklJQDSikoWSkoBpRRIWUUlAGUOkaUOkAUhkjJHnyeI4pbW22v9K2gPWkMkc2vFp+EN/VpE68Wv4TK+u2B19B0c/wvq7y3SpprW1rS13OnoCfb033No5XX9HmrJVqNrtpy+7R5v+oj/uz+z0B3WhWjirxDNPrX/wBSj09J4lVXMWl+Lsmu2mB72hGizQjQEWhGizRNoCLQlItSJtARpE6RakToCNIRlaJ0AmjBMA0lJEkpIDyVknJWQHkpKEkpIDSikoWUUkBkj5/BgVZvLptJ1S2vXZ9FKOJk/B1X/sl/s/8AkD3rwvBCbp00u7dVpD9LHS09RMNrvprb+4vjSflzr05rl/HY5fS78zHx/NznX8gfSxjS9El9EkPon1OecUO6+HZJerfsVjuk/dJ6fqANEpyy6qE91KTpe2zx9X4nqXMJrJupe/0pfE53Q5vLyzTfZvVfNP4gd6scv1lP6pM4HSSq6laWlzp6XwS2fQZnqaftLf2OJ4NO8rftLf8AIHXaJtFmidICVInSLMnQEaROkWonQEaJ0WolQEqJ0VonQCGCAB5KSTkpIFJKyTkpIFJKSTkrIDyUkSSkgPJxvF51m37zL/g7UnM8bjvjr5NAdaUrlbSaqVtPummLg6LFjfKYSfv66+gnQ5F5EU32ULb+iOXPiWVO2n2vek/0+zQD+LdRzvivyx2+tfFnV8PyJ4Jbr0TTb+Gj51HZxZY/wdbWkk5el61vswORle6p73tvv7/MRoYDA609Tz6S2/zTLxv6+if8EfA4/wAx/wBK/uefH26fK/8AVkif4Wz3+DTrG370/sgPYxKKMnQE6J0VonQEqJ0UonQEqJ0VonQEqJ0UonQCGMYBpKSTkpIFZKIlJWQKSVklJSQKyURKSkgVk8XjM7xy/al90eyROswvJjqV6+q36bQHh6d1fS8Jcp8nLdVx1O9kV0kL82fGv6d0xY8MzP1lL60j04/B7+NSvptgSU9MvW8t/wBMqV9z1x1ijBXlQ0lSSdtV3fd9h8fg8/qtv6JI9nTdHjxppbfLW+T2gOO+u3+fDhr/AMdMDzYH64XP9F/7nbvpMVeuOf40ee/C8L/S19KYHJ6jNj8uYxq0ubt89e3ujq+GzrDHz2/uRrwfHv8APWvbse6ZUpSuyS0l8gAxKGYlAJROh6J0BOidFKJ0BOidFKJUBOidFKJ0AhjGAMlZJSUkCslJJSUkCslJJSUlgVkpJKWUlgVljpkkx0wKpjpkkxkwKph2TTDsB9gbF2DYBbFbM2K2AGxGwtiNgLROhqYlMBKJ0PTJ0wEonQ9EqASidD0ToAGAYAoeWSkpLAqikslLHlgWllJZGWUlgTnrV57wudfhlze+zp7fH5PSf8MGPr7tY/Lxqslw7c1fGJlVrvWn8fkLfScrytvSucXBr80XHJ8vuhMHSZcc4nFY3kjG8dquSik65bTS2nv5AdDpeq5xVcXNQ6m4b3q59Vv4r5/Mjh8T3eCHGlnwrJy5bU0/SfT5PuN0uBxFqqVXkq7ppanlXwXy9Dzrw6+MLlKqOnjHL79s0UqmvT02gPb0/Xc8+XEp1OKZfPf5q+K18gdP4nNY8uSpcLEnevV3ia3Nr69zzLw+0qU3O7wTjqu6fN26yX6fHk9DV4WltRd8MmK8WRZMt5KS/Q55b1p77fMD14OqzNrzMCiamqmpyeZrS3q+y4v6bF8P6zNmUVWPDOO55fhz1eRJrt+Hgv7nnx9FleWMmR4k4ip5Y3k3k3OltPtPv8QeF9DeBxvD0k8Z41lxprNXb34re3rfcD29b13lVjnjy5PdvevLx7S5v96n7+w3+LSyZYtKVjicqre+UPe38tNf2PL1HhyzXlrLdpVKxwseXJC8rXpSTW+7r7CdT0OTLGFVcq5Xl5mt6yYnrkl83xX8sCleJtTgdY9PKlWRcv8AJxtpKn296n7+xut8Q8rz/wAHLycOPL+bXLlVLXp2/L9xOo8OWW8tZLtKkoiceW4SxpelJaT7umRz9DluMqqo55Omw4d7rXOapun29HyQHS6jKom7r0iXT/Y8F+IUsHm+WlatY6x1elNu1OnWvnv0DnxZ8y4ZfKiHcuvKyZOTld9J6Wu+vuefqPDq45YilUZHivWe7yN3NLe297TSS/YB68QpLJzxyqxvEnwyc4at67Vpd17a9inXdRWNRxmad5FjXK+Ers3tvT9jxvw++OXSwYnfl8ceLlOLc1vk+3q/T0G6vBmyzKuenfDIr4t3UUuNLT3PzQHpw3bTdzEvfbhbta+rSDTJdLjcTxcYsffanD2n6+i7j0wFpk6Y1MSmAlE2PTJsABFMBpY8skmOmBaWOmSTHTAtLKJkUx0wLJjpkUx0wLJjpkUxkwLJjJkUxkwK7DslsPICmwbE5A2A7YrYuwNgFsVsDYjYBbEbM2I2AGxKYWybYAbEphbJ0wA2TbGbEbABgGACY6ZJMdMCqZRMimOmBZMdMimOmBZMdMimMmBZMdMimMmBVMbZFMbkBXZtk+RuQFNm2T2bkA+wNibA2AzYrYrYrYBbFbA2I2AWxGzNiNgBsRsLYjYAbEbC2I2BjC7CAqY6ZJMdMCqY6ZFMdMCqY6ZFMdMCqY6ZFMZMCyYyZFMZMCqY2yKYeQFdh2S5B5AU2bZPYOQFNgbE5CtgO2K2K2BsAtitgbEbALYrYGxWwM2I2ZsVsANitmbFbA2zC7MAExkyaYyYFUxkySYyYFUxkySYyYFkxkyKYyYFUxkySYUwKqgpkth5AV2bZPkbkBTZtk+RuQD7A6E5A2A7YGxGwNgM2K2K2BsAtitgbFbAzYrZmxWwM2K2ZsVsDbMDZgAhkYwDIZGMAyCjGAZDIxgGQUYwBQQGAJjGAxjGAzAYwAYGYwCsDMYBWKwmARisxgFYGYwAMYwH/9k=";

if (form) {
	var timeout = null;
}

function keyupAction() {
	clearTimeout(timeout);
	timeout = setTimeout(function () {
		resultHolder.innerHTML = "";
		var searchTerm = document.querySelector("#bookname").value;
		var query_url = protocol + "//" + host + "/bookquery/" + searchTerm;
		if (searchTerm !== "") {
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function () {
				if (this.readyState == 4 && this.status == 200) {
					var first = JSON.parse(xhttp.responseText);

					// Setup the parent element
					var result = first.foreign;
					var parent = document.createElement("div");
					parent.classList.add("result-container-box");
					var listSet = document.createElement("ul");
					var cachedBooks = [];
					var x;
					if (!result.result.items) {
						if (first.own.length >= 1) {
							weHaveResults(first, listSet, parent, cachedBooks);
							noSatisfyingResults(listSet, parent);
						} else {
							noSatisfyingResults(listSet, parent);
							displayResults(parent, listSet);
						}
					} else {
						bookDepoResults(first, result, parent, listSet, cachedBooks);
						noSatisfyingResults(listSet, parent);
					}
				}
			};
			xhttp.open("GET", query_url, true);
			xhttp.send();
		}
	}, 500);
}


function weHaveResults(first, listSet, parent, cachedBooks) {

	console.log("OWN", first.own);
	let ownResults = first.own;
	if (ownResults.length > 10) {
		ownResults.splice(5, ownResults.length);
	}
	// Create a list 
	ownResults.forEach(function (book) {
		var listItem = document.createElement("li");
		var infoContainer = document.createElement("div");
		infoContainer.classList.add("book-info");

		// Title
		var itemTitle = document.createElement("p");
		var titleContent = document.createTextNode(book.title);
		itemTitle.appendChild(titleContent);
		infoContainer.appendChild(itemTitle);

		// Author
		var itemAuthor = document.createElement("p");
		var authorContent = document.createTextNode(book.author);
		itemAuthor.appendChild(authorContent);
		infoContainer.appendChild(itemAuthor);
		listItem.appendChild(infoContainer);

		// Image
		var imageContainer = document.createElement("div");
		imageContainer.classList.add("image-container");
		var image = document.createElement("img");
		image.src = book.image;
		imageContainer.appendChild(image);
		listItem.appendChild(imageContainer);

		// ISBN
		var isbnContainer = document.createElement("input");
		isbnContainer.setAttribute("type", "hidden");
		isbnContainer.value = book.isbn;
		infoContainer.appendChild(isbnContainer);

		// Image URL
		var imageTemp = document.createElement("input");
		imageTemp.setAttribute("type", "hidden");
		imageTemp.value = book.image;
		infoContainer.appendChild(imageTemp);

		// Append list element into list container 
		listSet.appendChild(listItem);

		// // Remember the isbn
		// cachedBooks.push(book.isbn);


	});

	// lengthChecker -= ownResults.length;

	parent.appendChild(listSet);
	resultHolder.appendChild(parent);
	resultHolder.style.display = "block";

	// Use selected values in the form

	var everyLi = document.getElementsByTagName("li");
	for (var y = 0; y < everyLi.length; y++) {
		everyLi[y].addEventListener('click', function (event) {
			var newTarget = event.target;
			if (newTarget.tagName !== "LI") {
				if (newTarget.tagName !== "DIV") {
					newTarget = newTarget.parentElement;
				}
				newTarget = newTarget.parentElement;
			}
			var titleHolder = newTarget.children[0].children[0].textContent;
			var authorHolder = newTarget.children[0].children[1].textContent;
			// var isbnHolder = newTarget.children[0].children[2].value;
			var imageHolder = newTarget.children[0].children[3].value;
			authorField.value = authorHolder;
			authorField.removeAttribute("disabled");
			if (authorLabel) {
				authorLabel.classList.add("active");
			}
			titleField.value = titleHolder;
			titleField.removeAttribute("disabled");
			if (titleLabel) {
				titleLabel.classList.add("active");
			}
			// isbnField.value = isbnHolder;
			// imageField.value = imageHolder;
			resultHolder.style.display = "none";
			// Submit the form if submitpick is found
			if (submitpick) {
				submitpick.submit();
			}
		});
	}
}



function bookDepoResults(first, result, parent, listSet, cachedBooks) {

	var lengthChecker = result.result.items[0].item.length;
	if (lengthChecker > 5) {
		lengthChecker = 5;
	}

	// If we have own results, loop them through and append to the list
	if (first.own) {
		console.log("OWN", first.own);
		let ownResults = first.own;
		if (ownResults.length > 5) {
			ownResults.splice(5, ownResults.length);
		}
		// Create a list 
		ownResults.forEach(function (book) {
			var listItem = document.createElement("li");
			var infoContainer = document.createElement("div");
			infoContainer.classList.add("book-info");

			// Title
			var itemTitle = document.createElement("p");
			var titleContent = document.createTextNode(book.title);
			itemTitle.appendChild(titleContent);
			infoContainer.appendChild(itemTitle);

			// Author
			var itemAuthor = document.createElement("p");
			var authorContent = document.createTextNode(book.author);
			itemAuthor.appendChild(authorContent);
			infoContainer.appendChild(itemAuthor);
			listItem.appendChild(infoContainer);

			// Image
			var imageContainer = document.createElement("div");
			imageContainer.classList.add("image-container");
			var image = document.createElement("img");
			image.src = book.image;
			imageContainer.appendChild(image);
			listItem.appendChild(imageContainer);

			// ISBN
			var isbnContainer = document.createElement("input");
			isbnContainer.setAttribute("type", "hidden");
			isbnContainer.value = book.isbn;
			infoContainer.appendChild(isbnContainer);

			// Image URL
			var imageTemp = document.createElement("input");
			imageTemp.setAttribute("type", "hidden");
			imageTemp.value = book.image;
			infoContainer.appendChild(imageTemp);

			// Append list element into list container 
			listSet.appendChild(listItem);

			// // Remember the isbn
			// cachedBooks.push(book.isbn);
		});

		lengthChecker -= ownResults.length;
	}
	for (x = 0; x < lengthChecker; x++) {
		let oldIsbnCheck = false;
		cachedBooks.forEach(function (item) {
			if (item === result.result.items[0].item[x].identifiers[0].isbn13[0]) {
				oldIsbnCheck = true;
			}
		});
		if (!oldIsbnCheck) {
			var listItem = document.createElement("li");

			var infoContainer = document.createElement("div");
			infoContainer.classList.add("book-info");

			// Title
			var itemTitle = document.createElement("p");
			var titleContent = document.createTextNode(result.result.items[0].item[x].biblio[0].title[0]);
			itemTitle.appendChild(titleContent);
			infoContainer.appendChild(itemTitle);

			// Author
			var itemAuthor = document.createElement("p");
			if (result.result.items[0].item[x].contributors) {
				var authorContent = document.createTextNode(result.result.items[0].item[x].contributors[0].contributor[0].name);
			} else {
				var authorContent = document.createTextNode("-");
			}

			itemAuthor.appendChild(authorContent);
			infoContainer.appendChild(itemAuthor);

			listItem.appendChild(infoContainer);
			// Image
			var imageContainer = document.createElement("div");
			imageContainer.classList.add("image-container");
			var image = document.createElement("img");
			if (result.result.items[0].item[x].images) {
				image.src = result.result.items[0].item[x].images[0].image[0]._;
			} else {
				image.src = noImage;
			}
			imageContainer.appendChild(image);
			listItem.appendChild(imageContainer);

			// ISBN
			var isbnContainer = document.createElement("input");
			isbnContainer.setAttribute("type", "hidden");
			isbnContainer.value = result.result.items[0].item[x].identifiers[0].isbn13[0];
			infoContainer.appendChild(isbnContainer);

			// Image URL
			var imageTemp = document.createElement("input");
			imageTemp.setAttribute("type", "hidden");
			if (result.result.items[0].item[x].images) {
				if (result.result.items[0].item[x].images[0].image[1]) {
					if (result.result.items[0].item[x].images[0].image[1]._) {
						imageTemp.value = result.result.items[0].item[x].images[0].image[1]._;
					}
				}
			} else {
				imageTemp.value = noImage;
			}
			infoContainer.appendChild(imageTemp);

			// Append list element into list container 
			listSet.appendChild(listItem);
		}
	}
	parent.appendChild(listSet);
	resultHolder.appendChild(parent);
	resultHolder.style.display = "block";

	// Use selected values in the form

	var everyLi = document.getElementsByTagName("li");
	for (var y = 0; y < everyLi.length; y++) {
		everyLi[y].addEventListener('click', function (event) {
			var newTarget = event.target;
			if (newTarget.tagName !== "LI") {
				if (newTarget.tagName !== "DIV") {
					newTarget = newTarget.parentElement;
				}
				newTarget = newTarget.parentElement;
			}
			var titleHolder = newTarget.children[0].children[0].textContent;
			var authorHolder = newTarget.children[0].children[1].textContent;
			var isbnHolder = newTarget.children[0].children[2].value;
			var imageHolder = newTarget.children[0].children[3].value;
			authorField.value = authorHolder;
			authorField.removeAttribute("disabled");
			if (authorLabel) {
				authorLabel.classList.add("active");
			}
			titleField.value = titleHolder;
			titleField.removeAttribute("disabled");
			if (titleLabel) {
				titleLabel.classList.add("active");
			}
			isbnField.value = isbnHolder;
			imageField.value = imageHolder;
			resultHolder.style.display = "none";
			// Submit the form if submitpick is found
			if (submitpick) {
				submitpick.submit();
			}
		});
	}

}

// This is the div where we show the add manually button

function noSatisfyingResults(listSet) {
	// Create an empty element with some class
	var emptyParent = document.createElement("div");
	emptyParent.classList.add("no-results-holder");
	// Create a text node
	var emptyParentContent = document.createTextNode("Etkö löytänyt hakemaasi?");
	var emptyParentContentHolder = document.createElement("p");
	emptyParentContentHolder.appendChild(emptyParentContent);
	emptyParent.appendChild(emptyParentContentHolder);
	// Create button
	var emptyParentButton = document.createElement("div");
	emptyParentButton.setAttribute("class", "no-good-results");
	emptyParentButton.classList.add("button");
	emptyParentButton.setAttribute("onclick", "displayManualThings()")
	var emptyParentButtonText = document.createTextNode("Lisää tiedot itse");
	emptyParentButton.appendChild(emptyParentButtonText);
	// Append button to holder
	emptyParent.appendChild(emptyParentButton);


	listSet.appendChild(emptyParent);

}

// This is called when we want to show results

function displayResults(parent, listSet) {
	parent.appendChild(listSet);
	resultHolder.appendChild(parent);
	resultHolder.style.display = "block";
}

// This is what happens when the button "Add manually" is clicked

function displayManualThings() {
	bookname.style.opacity = "0";
	bookname.style.transform = "scale(0)";
	file.style.opacity = "1";
	file.style.transform = "scale(1)";
	// isbn.type = "text";
	// isbnHid.style.display = "block";
	// isbnhint.style.display = "block";
	author.disabled = false;
	title.disabled = false;
	manualImage.required = true;
	resultHolder.style.display = "none";
}

// Hide on click anywhere but results

$(document).click(function (event) {
	if (!$(event.target).closest('#results').length) {
		if ($('#results').is(":visible")) {
			$('#results').hide();
		}
	}
});