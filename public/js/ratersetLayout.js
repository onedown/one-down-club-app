document.addEventListener('DOMContentLoaded', function () {
	//or for example
	var options = {
		max_value: 5,
		step_size: 0.5,
		readonly: true,
		// symbols: {
		// 	fontawesome_star: {
		// 		base: '<i class="far fa-star"></i>',
		// 		hover: '<i class="fas fa-star"></i>',
		// 		selected: '<i class="fas fa-star"></i>',
		// 	}
		// },
		// selected_symbol_type: 'fontawesome_star'
	}
	$(".rating").rate(options);

	const rateField = document.querySelector("#rateField");

	$(".rating").on("change", function (ev, data) {
		rateField.value = data.to;
	});
});