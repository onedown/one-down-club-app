document.addEventListener('DOMContentLoaded', function () {
  var toggle = document.querySelector("#manually");
  var bookname = document.querySelector("#bookname-container");
  var author = document.querySelector("#authorField");
  var title = document.querySelector("#titleField");
  var file = document.querySelector("#file-container");
  var isbn = document.querySelector("#isbn");
  var isbnHid = document.querySelector(".isbn-hid");
  var manualImage = document.querySelector("input[name=manualPic]");
  var isbnhint = document.querySelector(".isbn-hint");
  var bookpicker = document.querySelector("#submitpick");
  var rating = document.querySelector('.rating');
  var nogood = document.querySelector('.no-good-results');

  // if (toggle && rating) {
  //   // Prevent from submitting with no rating

  //   toggle.addEventListener('change', function () {
  //     if (toggle.checked) {
  //       displayManualThings();
  //     } else {
  //       bookname.style.opacity = "1";
  //       bookname.style.transform = "scale(1)";
  //       file.style.opacity = "0";
  //       file.style.transform = "scale(0)";
  //       isbn.type = "hidden";
  //       isbnHid.style.display = "none";
  //       isbnhint.style.display = "none";
  //       author.disabled = true;
  //       title.disabled = true;
  //       manualImage.required = false;
  //     }
  //   });
  // } else
  if (bookpicker) {

    toggle.addEventListener('change', function () {
      if (toggle.checked) {
        bookname.style.opacity = "0";
        bookname.style.transform = "scale(0)";
        file.style.opacity = "1";
        file.style.transform = "scale(1)";
        author.disabled = false;
        title.disabled = false;
        author.type = "text";
        title.type = "text";

      } else {
        bookname.style.opacity = "1";
        bookname.style.transform = "scale(1)";
        file.style.opacity = "0";
        file.style.transform = "scale(0)";
        author.disabled = true;
        title.disabled = true;
        author.type = "hidden";
        title.type = "hidden";
      }
    });

  }

  // function displayManualThings() {
  //   bookname.style.opacity = "0";
  //   bookname.style.transform = "scale(0)";
  //   file.style.opacity = "1";
  //   file.style.transform = "scale(1)";
  //   isbn.type = "text";
  //   isbnHid.style.display = "block";
  //   isbnhint.style.display = "block";
  //   author.disabled = false;
  //   title.disabled = false;
  //   manualImage.required = true;
  // }

});