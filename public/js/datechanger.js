document.addEventListener('DOMContentLoaded', function () {
  var datePlace = document.getElementsByClassName('date-place');
  for (var x = 0; x < datePlace.length; x++) {
    var tmp = datePlace[x].textContent;
    var newTmp = tmp.split("GMT", 1);
    newerTmp = newTmp[0].split(/\s+/g);
    for (var y = 0; y < newerTmp.length; y++) {
      if (newerTmp[0] == "") {
        newerTmp.shift();
        break;
      }
    }

    var months = ["null", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    for (var y = 0; y < months.length; y++) {
      if (newerTmp[1] == months[y]) {
        if (months.indexOf(months[y]) < 10) {
          newerTmp[1] = "0" + months.indexOf(months[y]);
        } else {
          newerTmp[1] = months.indexOf(months[y]);
        }
      }
    }

    actualTmp = newerTmp[2] + "." + newerTmp[1] + "." + newerTmp[3];
    datePlace[x].innerHTML = actualTmp;
  }
});