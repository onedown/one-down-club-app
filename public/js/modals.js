document.addEventListener('DOMContentLoaded', function () {
  const modal = document.getElementsByClassName("modal");
  const modalContainer = document.querySelector(".modal-container");
  let timer = 1500;

  if (modal && modalContainer) {
    for (let x = 0; x < modal.length; x++) {
      const animateModal = function () {
        modal[x].style.opacity = "0";
        modal[x].style.transform = "translate(0px, -50px)";
      }
      setTimeout(animateModal, timer);
      timer += 200;
    }
    setTimeout(function () {
      modalContainer.style.display = "none";
    }, timer + 100);
  }

  const trigger = document.querySelector("#pw-confirm");
  const confirmation = document.querySelector("#modal1");
  if (trigger) {
    trigger.addEventListener('click', function () {
      confirmation.style.display = "block";
    });
  }

});