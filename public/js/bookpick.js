document.addEventListener('DOMContentLoaded', function () {
  var editButton = document.querySelector(".picks-mypick");
  var picker = document.querySelector(".bookpicker");
  var editpick = document.querySelector(".editpick");
  var cancel = document.querySelector('#cancelBookpick');

  if (editButton) {
    editButton.addEventListener('click', function () {
      picker.style.display = "block";
    });
  }

  if (editpick) {
    editpick.addEventListener('click', function () {
      picker.style.display = "block";
    });
  }

  if (cancel) {
    cancel.addEventListener('click', function () {
      picker.style.display = "none";
    });
  }
});