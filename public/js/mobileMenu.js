document.addEventListener('DOMContentLoaded', function () {
  var menuButton = document.querySelector(".navigation-menu-button");
  var menu = document.querySelector(".navigation");

  menuButton.addEventListener('click', function () {
    if (menu.style.left === "-100%") {
      menu.style.left = "0%";
    } else {
      menu.style.left = "-100%";
    }
  });
});