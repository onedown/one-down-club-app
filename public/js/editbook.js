document.addEventListener('DOMContentLoaded', function () {
  var editButton = document.querySelector(".edit-book");
  var thoughts = document.querySelector(".my-thoughts");
  var thoughtContainer = document.querySelector(".thoughts-edit-place");
  var removeButton = document.querySelector(".remove-book");
  var singleDelete = document.querySelector(".single-delete");

  if (editButton) {
    editButton.addEventListener('click', function () {
      singleDelete.style.display = "none";
      // Create form
      let formCont = document.createElement("form");
      formCont.setAttribute("method", "post");
      formCont.setAttribute("action", "/bookedit");
      // Create textarea
      let textField = document.createElement("textarea");
      textField.classList.add("edit-field");
      textField.setAttribute("name", "thoughts");
      textField.value = thoughts.textContent;
      // Textarea to form
      formCont.appendChild(textField);

      // Create hidden field for id
      let hiddenId = document.createElement("input");
      hiddenId.setAttribute("type", "hidden");
      hiddenId.setAttribute("name", "bookId");
      let path = location.pathname;
      let pathSplit = path.split("/");
      let pathTmp = pathSplit.length - 1;
      let pathUse = pathSplit[pathTmp];
      hiddenId.value = pathUse;
      formCont.appendChild(hiddenId);

      // Create submit
      let submitButton = document.createElement("input");
      submitButton.setAttribute("type", "submit");
      submitButton.setAttribute("value", "Save");
      submitButton.classList.add("button");

      // Button to form
      formCont.appendChild(submitButton);

      // Form to container
      thoughtContainer.appendChild(formCont);
      // Empty thougts, edit buttons
      thoughts.innerHTML = '';
      editButton.style.display = "none";
    });
  }

  if (removeButton) {
    removeButton.addEventListener('click', function () {
      let formCont = document.createElement("form");
      formCont.setAttribute("method", "post");
      formCont.setAttribute("action", "/bookremove");
    });
  }
});