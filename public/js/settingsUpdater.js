document.addEventListener('DOMContentLoaded', function () {
  const onGroupPostToggle = document.querySelector("#onGroupPostToggle"),
    onGroupPickToggle = document.querySelector("#onGroupPickToggle"),
    onNewCommentToggle = document.querySelector("#onNewCommentToggle");

  if (onGroupPickToggle && onGroupPostToggle && onNewCommentToggle) {
    let host = window.location.hostname;
    if (host === "localhost") {
      host = host + ":3000";
    }
    console.log("HOST", host);

    let newSettings = {};

    onGroupPostToggle.addEventListener('change', () => {
      if (this.checked) {
        newSettings = {
          onGroupPost: true,
        }
      } else {
        newSettings = {
          onGroupPost: false,
        }
      }

      const url = location.protocol + host + "/settings/notification";
      console.log("URL", url);

      (async () => {
        const rawResponse = await fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(newSettings)
        });
        const content = await rawResponse;

        console.log(content);
      })();
    });

    onGroupPickToggle.addEventListener('change', () => {
      if (this.checked) {
        newSettings = {
          onGroupPost: true,
        }
      } else {
        newSettings = {
          onGroupPost: false,
        }
      }
    });

    onNewCommentToggle.addEventListener('change', () => {
      if (this.checked) {
        newSettings = {
          onNewComment: true,
        }
      } else {
        newSettings = {
          onNewComment: false,
        }
      }
    });
  }


});