var nodemailer = require("nodemailer");
var express = require('express');
var router = express.Router();

module.exports = router;

module.exports.mailerBoi = (recipients, subject, content) => {
  // Expects list of recipients, subject, message content (html)
  let transporter = nodemailer.createTransport({
    host: 'mail.onedown.club',
    port: 465,
    secure: 465,
    auth: {
      user: 'hello@onedown.club',
      pass: '(!sW0Co;(WCV'
    },
    tls: {
      rejectUnauthorized: false
    }
  });

  // setup email data with unicode symbols
  let mailOptions = {
    from: '"One Down Club" <hello@onedown.club>', // sender address
    to: recipients, // list of receivers
    subject: subject, // Subject line
    html: content // html body
  };

  // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log('Message sent: %s', info.messageId);
    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
  });
}

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}