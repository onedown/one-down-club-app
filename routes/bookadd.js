var express = require('express');
var router = express.Router();

var books = require('../models/book');
var codes = require('../models/code');
var dbooks = require('../models/dbook');
var activity = require('../models/activity');

var mailService = require('../util/mail');
const upload = require('../util/multer');

// ME OLLAAN JO SAATANA BOOKADDISSA, EI TARVI ENÄÄ UUDESTAAN LAITTAA VITTU
// According to google: WE HAVE BEEN OLD ON BOOKADD, YOU DO NOT REQUIRE YOURSELVES 
// XD
router.post('/', ensureAuthenticated, (req, res) => {

  const singleUpload = upload.single('manualPic');
  singleUpload(req, res, function (err, some) {
    if (err) {
      return res.status(422).send({
        errors: [{
          title: 'Image Upload Error',
          detail: err.message
        }]
      });
    }


    let bookToInsert = new books({
      title: req.body.title,
      author: req.body.author,
      thoughts: `${req.body.yourThought}`,
      owner: req.user.username,
      ownerId: req.user.id,
      isbn: req.body.isbn,
      image: req.body.imageField ? req.body.imageField : req.file.location,
      rating: req.body.rateField,
      date: Date.now(),
    });

    books.createBook(bookToInsert, function (err, createdBook) {
      if (err) throw err;

      // Notify

      // if (req.body.groupToPost) {
      //   // Send notifications
      //   // Get memebers of the group
      //   groups.getById(req.body.groupToPost, (err, results) => {
      //     if (err) throw err;
      //     console.log(results.members);
      //     // Loop through members, if they allow notifications 
      //     // on group post and it isn't you then post
      //     for (let x = 0; x < results.members.length; x++) {
      //       users.getByIdForSerialization(results.members[x].id, (err, user) => {
      //         if (err) throw err;
      //         if (user.emailSettings.onGroupPost && req.user.id !== user.id) {
      //           // Send mail
      //           mailService.mailerBoi(user.email, "New book on" + results.name, req.user.username + " added a new book");
      //         }
      //       });
      //     }
      //   });
      // }


      req.flash('success_msg', 'Book added!');
      res.render('add');

      // Create a new acitivity for every group

      let groups = req.body.groupToPost;

      if (req.body.groupToPost) {
        if (!(Array.isArray(groups))) {
          let newActivity = new activity({
            user: req.user.username,
            userId: req.user.id,
            company: req.user.company,
            companyId: req.user.companyId,
            type: "bookToGroup",
            groupId: req.body.groupToPost,
            link: createdBook.id,
            data: {
              bookTitle: req.body.title,
              bookAuthor: req.body.author,
              bookImage: req.body.imageField ? req.body.imageField : req.file.location,
              bookThoughts: req.body.yourThought,
              bookIsbn: req.body.isbn,
              bookRating: Number(req.body.rateField),
            },
          });

          activity.create(newActivity, (err, created) => {
            if (err) throw err;
            console.log(created);
          });
        } else {
          for (let x = 0; x < req.body.groupToPost.length; x++) {
            let newActivity = new activity({
              user: req.user.username,
              userId: req.user.id,
              company: req.user.company,
              companyId: req.user.companyId,
              type: "bookToGroup",
              groupId: req.body.groupToPost[x],
              link: createdBook.id,
              data: {
                bookTitle: req.body.title,
                bookAuthor: req.body.author,
                bookImage: req.body.imageField ? req.body.imageField : req.file.location,
                bookThoughts: req.body.yourThought,
                bookIsbn: req.body.isbn,
                bookRating: Number(req.body.rateField),
              },
            });

            activity.create(newActivity, (err, created) => {
              if (err) throw err;
              // console.log(created);
            });
          }
        }
      }

      // Create a new dbook

      let dbookFind = {
        $or: [{
          title: {
            "$regex": req.body.title,
            "$options": "i"
          },
        }, {
          author: {
            "$regex": req.body.author,
            "$options": "i"
          }
        }],
      }

      dbooks.getByIsbnCompany(dbookFind, (err, result) => {
        if (err) throw err;
        if (result.length < 1) {
          let dbookToInsert = new dbooks({
            title: req.body.title,
            author: req.body.author,
            // isbn: req.body.isbn,
            image: req.body.imageField ? req.body.imageField : req.file.location,
            avgRating: req.body.rateField,
            avgRatingSum: req.body.rateField,
            company: req.user.company,
            reviews: [{
              owner: req.user.username,
              ownerId: req.user.id,
              rating: Number(req.body.rateField),
              thoughts: req.body.yourThought,
              link: '/users/books/' + createdBook.id,
            }],
            bookId: createdBook.id
          });
          dbooks.createDBook(dbookToInsert, (err, inserted) => {
            if (err) throw err;
          });
        } else {
          // Get the sum of all the ratings
          let avgSum = 0;
          for (let x = 0; x < result[0].reviews.length; x++) {
            avgSum += result[0].reviews[x].rating;
          }
          console.log("Avg sum", avgSum);

          avgSum += Number(req.body.rateField);
          let lengthHolder = result[0].reviews.length;
          lengthHolder++;
          // Calculate the new average
          let newAverageRating = avgSum / lengthHolder;
          // Round the new average to the nearest 0.5
          newAverageRating = Math.round(newAverageRating * 2) / 2;
          let newReview = {
            $set: {
              avgRating: newAverageRating,
              avgRatingSum: avgSum,
            },
            $push: {
              reviews: {
                owner: req.user.username,
                ownerId: req.user.id,
                rating: Number(req.body.rateField),
                thoughts: req.body.yourThought,
              }
            }
          }

          let newQuery = {
            isbn: req.body.isbn,
            company: req.user.company,
          }

          dbooks.addDBook(newQuery, newReview, (err, inserted) => {
            if (err) throw err;

          });
        }
      });
    });
  });
});


function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}

module.exports = router;