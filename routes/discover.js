var express = require('express');
var router = express.Router();

var dbooks = require('../models/dbook');
var users = require('../models/user');

router.get('/', ensureAuthenticated, (req, res) => {
  dbooks.getByCompany(req.user.company, (err, dbooks) => {
    if (err) throw err;
    res.render('discover', {
      dbooks,
      title: res.__('discoverHeader'),
      discoverActive: true,
    });
  });
});

router.get('/:id', ensureAuthenticated, (req, res) => {
  dbooks.getById(req.params.id, (err, results) => {
    if (err) {
      res.render('error', {
        title: res.__('errorHeader'),
        errorRead: "That book was not found in our library 🤔"
      });
      return;
    }
    let renderCounter = 0;
    // Get review author's profile images
    for (let x = 0; x < results.reviews.length; x++) {
      users.getById(results.reviews[x].ownerId, (err, user) => {
        results.reviews[x].pic = user[0].pic;
        renderCounter++;
        tryRender();
        console.log("looped", x);
      });
    }
    const tryRender = () => {
      if (renderCounter === results.reviews.length) {
        res.render('discoversingle', {
          dbook: results,
          title: 'Discover Books - ' + results.title,
          discoverActive: true,
        });
      }
    }
  });
});

router.get('/companies/:id', ensureAuthenticated, ensureAdmin, (req, res) => {
  dbooks.getByCompany(req.params.id, (err, dbooksS) => {
    if (err) throw err;
    console.log('DBooks', dbooksS);
    res.render('discover', {
      dbooks: dbooksS,
      title: res.__('discoverHeader'),
      discoverActive: true,
    });
  });
});

module.exports = router;

function ensureAdmin(req, res, next) {
  if (req.user.admin) {
    return next();
  } else {
    req.flash('error_msg', 'You are not an admin :(');
    res.redirect('/');
  }
}

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}