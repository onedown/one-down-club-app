var express = require('express');
var router = express.Router();

var groups = require('../models/group');
var users = require('../models/user');

router.post('/:id', ensureAuthenticated, (req, res) => {
  let removeUserUse = {
    $pull: {
      groups: {
        id: req.params.id
      }
    }
  }

  let userIdToChange = {
    _id: req.user.id,
  }

  let removeUserGroupUse = {
    $pull: {
      members: {
        id: req.user.id
      },
      owners: {
        id: req.user.id
      }
    }
  }

  let groupIdUse = {
    _id: req.params.id,
  }

  users.addGroup(userIdToChange, removeUserUse, (err, updatedUser) => {
    if (err) throw err;
    groups.addUser(groupIdUse, removeUserGroupUse, (err, updated) => {
      if (err) throw err;
      console.log(updated);
    });
  });

  res.redirect('back');
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}