var express = require('express');
var router = express.Router();

var accessCode = require('../models/code');
var users = require('../models/user');
var toplist = require('../models/toplist');
const upload = require('../util/multer');

router.get('/', ensureAuthenticated, ensureModerator, (req, res) => {
  accessCode.getByCompanyId(req.user.companyId, (err, result) => {
    if (err) throw err;
    res.render('company', {
      title: res.__('companySettingsHeader'),
      companyActive: true,
      company: result,
    });
  });
});

router.post('/pic', ensureAuthenticated, ensureModerator, (req, res) => {
  let singleUpload = upload.single('companyCover');
  singleUpload(req, res, function (err, some) {
    if (err) {
      return res.status(422).send({
        errors: [{
          title: 'Image Upload Error',
          detail: err.message
        }]
      });
    }

    req.flash('success_msg', 'Company Cover Pic changed!');
    res.render('company');

    let picUse = {
      cover: req.file.location
    }

    accessCode.setAttribute(req.user.companyId, picUse, (err, updated) => {
      if (err) throw err;
      console.log(updated);
    });
  });
});

router.post('/logo', ensureAuthenticated, ensureModerator, (req, res) => {
  let singleUpload = upload.single('companyLogo');
  singleUpload(req, res, function (err, some) {
    if (err) {
      return res.status(422).send({
        errors: [{
          title: 'Image Upload Error',
          detail: err.message
        }]
      });
    }

    req.flash('success_msg', 'Company Logo changed!');
    res.render('company');

    let picUse = {
      logo: req.file.location
    }
    accessCode.setAttribute(req.user.companyId, picUse, (err, updated) => {
      if (err) throw err;
      console.log(updated);
      let userPicUse = {
        companyLogo: req.file.location
      }
      users.getByCompany(req.user.companyId, (err, results) => {
        if (err) throw err;
        console.log(results);
        for (let x = 0; x < results.length; x++) {
          users.setPic(results[x].id, userPicUse, (err, updatedUser) => {
            if (err) throw err;
          });
        }
      });
    });
  });
});

router.post('/color', ensureAuthenticated, ensureModerator, (req, res) => {
  let colorUse = {
    color: req.body.color,
  }
  accessCode.setAttribute(req.user.companyId, colorUse, (err, updated) => {
    if (err) throw err;
    res.render('company', {
      message: '{{i18n "colorSaved"}}'
    });
  });
});

router.get('/new', ensureAuthenticated, ensureModerator, (req, res) => {
  res.render('companycreate', {
    title: res.__('createCompanyHeader'),
    addCompanyActive: true,
  });
});

router.post('/new', ensureAuthenticated, ensureModerator, (req, res) => {
  let newCompany = new accessCode({
    accessCode: req.body.accessCode,
    company: req.body.company,
    color: req.body.color,
  });
  accessCode.createCompany(newCompany, (err, created) => {
    if (err) throw err;
    console.log(created);
    let newToplist = new toplist({
      company: created.company,
      companyId: created.id,
      users: {

      },
    });
    toplist.create(newToplist, (err, created) => {
      if (err) throw err;
      console.log(created);
      res.render('companycreate', {
        message: 'New Company Created!',
      })
    });
  });
});

router.get('/manage', ensureAuthenticated, ensureModerator, (req, res) => {
  res.render('companymanage', {
    title: res.__('manageCompanyHeader')
  });
});

function ensureModerator(req, res, next) {
  if (req.user.moderator) {
    return next();
  } else {
    req.flash('error_msg', 'You are not a moderator :(');
    res.redirect('/');
  }
}

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}

module.exports = router;