var express = require('express');
var router = express.Router();

var accessCode = require('../models/code');
var recs = require('../models/recs');
var reccats = require('../models/reccat');
var users = require('../models/user');
var books = require('../models/book');
const upload = require('../util/multer');

router.get('/companies', ensureAuthenticated, ensureAdmin, (req, res) => {

  accessCode.getAll((err, results) => {
    let counter = 1;
    if (err) throw err;
    results.forEach((company, index) => {
      users.getByCompany(company.id, (err, users) => {
        if (err) throw err;
        results[index]["userArray"] = users;
        tryRender();
      });
    });

    function tryRender() {
      if (counter === results.length) {
        res.render('companies', {
          title: "Every company",
          companies: results
        });
      } else {
        return counter++;
      }
    }
  });
});

router.get('/tracks', ensureAuthenticated, ensureAdmin, (req, res) => {
  recs.getAll((err, result) => {
    if (err) throw err;
    sortLowToHigh = (a, b) => {
      if (a.index < b.index)
        return -1;
      if (a.index > b.index)
        return 1;
      return 0;
    }
    result.sort(sortLowToHigh);

    reccats.getAll((err, reccats) => {
      if (err) throw err;
      console.log(reccats);
      res.render('tracksadd', {
        title: "Add reading tracks",
        tracksAddActive: true,
        result,
        reccats,
      });
    });
  });
});

router.post('/tracks/add', ensureAuthenticated, ensureAdmin, (req, res) => {
  const singleUpload = upload.single('trackPic');
  singleUpload(req, res, function (err, some) {
    if (err) {
      return res.status(422).send({
        errors: [{
          title: 'Image Upload Error',
          detail: err.message
        }]
      });
    }
    if (req.file) {
      let recUse = new recs({
        title: req.body.title,
        author: req.body.author,
        index: Number(req.body.index),
        desc: req.body.desc ? req.body.desc : "No description yet",
        category: req.body.category,
        image: req.file.location,
        link: req.body.link ? req.body.link : "https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=" + req.body.title,
      });
      recs.createRec(recUse, (err, created) => {
        if (err) throw err;
        req.flash('success_msg', 'Added!');
        res.redirect('back');
      });
    } else {
      res.render('error', {
        title: res.__('errorHeader'),
        errorRead: res.__('somTingWong') + " 😞"
      });
    }
  });
});

router.post('/tracks/remove/:id', ensureAuthenticated, ensureAdmin, (req, res) => {
  recs.destroy(req.params.id, (err, removed) => {
    if (err) throw err;
    console.log("Removed:", removed);
    req.flash('success_msg', 'Track removed!');
    res.redirect('back');
  });
});

router.post('/tracks/cat/add', ensureAuthenticated, ensureAdmin, (req, res) => {
  let reccatUse = new reccats({
    category: req.body.category,
    description: req.body.description,
  });
  reccats.create(reccatUse, (err, created) => {
    if (err) throw err;
    req.flash('success_msg', 'Category created!');
    res.redirect('back');
  });
});

router.post('/tracks/cat/edit', ensureAuthenticated, ensureAdmin, (req, res) => {
  let newContent = {
    description: req.body.newDesc,
  }
  reccats.editRec(req.body.recId, newContent, (err, done) => {
    if (err) throw err;
    req.flash('success_msg', 'Modified!');
    res.redirect('back');
  });
});

router.get('/stats', ensureAuthenticated, ensureAdmin, (req, res) => {
  accessCode.getAll((err, result) => {
    if (err) throw err;
    res.render('stats', {
      companies: result,
      adminStatsActive: true,
      title: "Statistics",
      adminStats: true
    });
  });
});

router.post('/stats/company', ensureAuthenticated, ensureAdmin, (req, res) => {
  accessCode.getAll((err, companies) => {
    if (err) throw err;
    users.getByCompany(req.body.companyId, (err, result) => {
      if (err) throw err;

      let bookResults = [];
      let totalBooks = 0;
      let checker = 0;
      let avgTotal = 0;

      for (let x = 0; x < result.length; x++) {
        books.getByOwnerId(result[x].id, (err, found) => {
          if (err) throw err;
          // Calculate words function
          function count_words(str1) {
            //exclude  start and end white-space
            str1 = str1.replace(/(^\s*)|(\s*$)/gi, "");
            //convert 2 or more spaces to 1  
            str1 = str1.replace(/[ ]{2,}/gi, " ");
            // exclude newline with a start spacing  
            str1 = str1.replace(/\n /, "\n");
            return str1.split(' ').length;
          }
          // Compose review average lengths array
          let avgHolder = 0;
          // Sum up char count of every review
          found.forEach(element => {
            avgHolder += count_words(element.thoughts);
          });
          // Calculate the avg
          avgHolder = avgHolder / found.length;
          // Compose bookResults array and count total books and avg
          let object = {};
          object["id"] = result[x].id;
          object["name"] = result[x].username;
          object["count"] = found.length;
          object["avg"] = avgHolder ? Math.round(avgHolder) : "-";
          object["groups"] = result[x].groups.length;
          avgTotal += avgHolder ? avgHolder : 0;
          bookResults[x] = object;
          totalBooks += found.length;
          logger();
          checker++;
        });
      }

      function logger() {
        if (checker === result.length) {
          // Calculate the total avg
          avgTotal = avgTotal / result.length;
          avgTotal = Math.round(avgTotal);
          // Render page
          res.render('stats', {
            stats: result[0],
            userTotal: result.length,
            adminStatsActive: true,
            title: "Statistics for " + (result[0] ? result[0].company : "no-one"),
            bookResults,
            totalBooks,
            avgTotal,
            companies: companies,
            adminStats: true,
          });
        }
      }
    });
  });
});

module.exports = router;

function ensureAdmin(req, res, next) {
  if (req.user.admin) {
    return next();
  } else {
    req.flash('error_msg', 'You are not an admin :(');
    res.redirect('/');
  }
}

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}