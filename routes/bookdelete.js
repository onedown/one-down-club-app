var express = require('express');
var router = express.Router();

var books = require('../models/book');
var dbooks = require('../models/dbook');
var activity = require('../models/activity');


router.post('/', ensureAuthenticated, (req, res) => {
  // Check if this book is the only one in the dbook
  // and if so, delete the dbook
  dbooks.getByBookId(req.body.removeId, (err, found) => {
    if (err) throw err;
    if (found[0].reviews.length < 2) {
      dbooks.destroyByBookId(req.body.removeId, (err, found) => {
        if (err) throw err;
        console.log(found);
      });
    }
  });

  // Destroy the book
  let rmId = req.body.removeId;
  books.destroy(rmId, (err, deleted) => {
    if (err) throw err;

    let idUse = {
      link: rmId
    }
    // Destroy all the activities
    activity.destroyByBookId(idUse, (err, deletedAct) => {
      if (err) throw err;

    });
  });
  res.redirect('/userBooks/' + req.user.username);
  req.flash('success_msg', 'Book deleted!');
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}