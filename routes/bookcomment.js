var express = require('express');
var router = express.Router();
var fetch = require('node-fetch');

var books = require('../models/book');

router.post('/', ensureAuthenticated, (req, res) => {
  let newComment = {
    $push: {
      comments: {
        userId: req.user.id,
        userName: req.user.username,
        userPic: req.user.pic,
        comment: req.body.commentField,
        timestamp: new Date(new Date().toUTCString()),
      }
    }
  }

  books.addComment(req.body.bookId, newComment, (err, added) => {
    if (err) throw err;
    console.log(added);
    req.flash('success_msg', 'Comment added!');
    res.redirect('back');
  });


  //Send notification
  const query_url = "http://localhost:3000/slack/bookCommentNotify/" + req.body.bookId
  fetch(query_url)
        .then(response => console.log(response))
        .catch(error => {
          console.error(error);
        });
});

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}

module.exports = router;