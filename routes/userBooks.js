var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();

var books = require('../models/book');


router.get('/:username', ensureAuthenticated, (req, res) => {
  books.getByUsername(req.params.username, function (err, books) {
    if (err) throw err;
    books = books.reverse();
    res.render('bookshelf', {
      title: res.__('thisusersBookshelfHeader') + " " + req.params.username,
      layout: "bookshelf",
      books,
      bookshelfOwner: req.params.username,
      bookshelfActive: true,
    });
  });
});

router.get('/me', ensureAuthenticated, (req, res) => {
  console.log("Username", req.user.username);
  books.getByUsername(req.user.username, function (err, books) {
    if (err) throw err;
    res.send(books);
  });
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}