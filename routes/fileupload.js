var express = require('express');
var router = express.Router();

const upload = require('../util/multer');
const singleUpload = upload.single('image');


// const uploadParser = (form) => {
//   var form = new formidable.IncomingForm();
//   // I hate this
//   form.uploadDir = "./public/uploads";
//   form.keepExtensions = true;
//   form.maxFileSize = 100 * 1024 * 1024;
//   form.parse(req, (err, fieldsRes, filesRes) => {
//     if (err) throw err;
//     console.log("Fields", fieldsRes);
//     console.log("Files", filesRes);
//     res.render('settings', {

//     });
//     return;
//   });

//   form.on('file', (name, file) => {

//     console.log("NAME:", name);


//     var AWS = require('aws-sdk');
//     var s3 = new AWS.S3();

//     var params = {
//       Key: file.path,
//       Bucket: process.env.BUCKETEER_BUCKET_NAME,
//       Body: new Buffer([file]),
//     };

//     s3.putObject(params, function put(err, data) {
//       if (err) {
//         console.log(err, err.stack);
//         return;
//       } else {
//         console.log(data);
//       }

//       delete params.Body;
//       s3.getObject(params, function put(err, data) {
//         if (err) console.log(err, err.stack);
//         else console.log(data);

//         console.log(data.Body.toString());
//       });
//     });


//     let filePlace = file.path;
//     console.log("PATH", file.path);

//     filePlace = filePlace.replace('public/', '');
//     console.log("POST PATH", file.path);
//     console.log(filePlace);
//     let picUse = {
//       pic: "/" + filePlace
//     }

//     users.setPic(req.user.id, picUse, (err, user) => {
//       if (err) throw err;
//       console.log(user);
//     });
//   });
// }

router.post('/', function (req, res) {
  singleUpload(req, res, function (err, some) {
    if (err) {
      return res.status(422).send({
        errors: [{
          title: 'Image Upload Error',
          detail: err.message
        }]
      });
    }

    return res.json({
      'imageUrl': req.file.location
    });
  });
})

module.exports = router;