var express = require('express');
var fetch = require('node-fetch');
var router = express.Router();
const cors = require('cors');
const parseString = require('xml2js').parseString;
require('es6-promise').polyfill();
require('isomorphic-fetch');

var dbooks = require('../models/dbook');

router.get('/:searchTerm', ensureAuthenticated, (req, res) => {

  // Let's create a common object for both results
  let combined = {};

  // Search our own db for the book
  dbooks.getByName(req.params.searchTerm, (err, found) => {
    if (err) throw err;
    console.log("OWN BOOKS: ", found);

    // Search bookdepository for the book
    // API URL construction
    const API_URL = "https://api.bookdepository.com/search/books?clientId=";
    const clientId = "ade821e7";
    const apiKey = "a537d3fe07e2c8bf2eb1089399fb1b8b28a0e0d0";
    const clientIp = "84.250.161.164";
    const query_url = API_URL + clientId + "&authenticationKey=" + apiKey + "&IP=" + clientIp + "&keywords=" + req.params.searchTerm + "&images=small,large";

    fetch(query_url)
      .then(response => response.text())
      .then(results => {
        parseString(results, (err, result) => {
          if (found) {
            combined["own"] = found;
            combined["foreign"] = result;
            res.json(combined);
          } else {
            combined["foreign"];
            res.json(combined);
          }
        });
      }).catch(error => {
        console.error(error);
      });
  });

});


function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}

module.exports = router;