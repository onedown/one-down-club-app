var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var users = require('../models/user');
var accessCode = require('../models/code');

router.get('/', ensureAdmin, (req, res) => {
  res.render('adminregister', {
    adminsActive: true,
  });
});

// Register User
router.post('/', ensureAdmin, function (req, res) {
  var email = req.body.email;
  var username = req.body.username;
  var password = req.body.password;
  var accessCodeForm = req.body.accessCodeForm;
  var moderatorPriviledge = req.body.moderatorPriviledge;
  var adminPriviledge = req.body.adminPriviledge;


  let modPrivBoolean;
  let adminPrivBoolean;
  // Turn form "on" values into booleans
  if (moderatorPriviledge == "on") {
    modPrivBoolean = true
  } else {
    modPrivBoolean = false
  }

  if (adminPriviledge == "on") {
    adminPrivBoolean = true
  } else {
    adminPrivBoolean = false
  }

  // Validation
  req.checkBody('email', 'Email is required').notEmpty();
  req.checkBody('email', 'Email is not valid').isEmail();
  req.checkBody('username', 'Username is required').notEmpty();
  req.checkBody('password', 'Password is required').notEmpty();
  req.checkBody('accessCodeForm', 'Code is required').notEmpty();

  var errors = req.validationErrors();

  if (errors) {
    res.render('register', {
      errors: errors
    });
  } else {
    //checking if email and username are already taken
    // some of the validation is broken
    accessCode.findOne({
      accessCode: {
        $regex: accessCodeForm
      }
    }).then(doc => {
      console.log("DOC", doc);

      if (doc) {
        users.findOne({
          username: {
            "$regex": "^" + username + "\\b",
            "$options": "i"
          }
        }, function (err, user) {
          users.findOne({
            email: {
              "$regex": "^" + email + "\\b",
              "$options": "i"
            }
          }, function (err, mail) {
            if (user || mail) {
              res.render('register', {
                user: user,
                mail: mail
              });
            } else {
              var newUser = new users({
                admin: adminPrivBoolean,
                moderator: modPrivBoolean,
                email: email,
                username: username,
                password: password,
                company: doc.company,
                admin: adminPrivBoolean,
                moderator: modPrivBoolean,
                companyId: doc._id,
                companyLogo: "https://onedown.club/wp-content/uploads/2018/05/OneDown_ilman_taustaa_valk_teksti.png",
                pic: "http://www.viikonloppu.com/wp-content/uploads/2014/04/slide_245969_1407896_free-619x464.jpg",
                emailSettings: {
                  onGroupPost: true,
                  onGroupPick: false,
                  onNewComment: true,
                }
              });
              users.create(newUser, function (err, user) {
                if (err) throw err;
                console.log(user);
              });
              req.flash('success_msg', 'You are registered and can now login');
              res.redirect('back');
            }
          });
        });
      }

    }).catch(err => {
      console.error(err);
    });

    return;
  }
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    //req.flash('error_msg','You are not logged in');
    res.redirect('/users/login');
  }
}

function ensureAdmin(req, res, next) {
  if (req.user.admin) {
    return next();
  } else {
    req.flash('error_msg', 'You are not an admin :(');
    res.redirect('/');
  }
}