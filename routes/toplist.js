var express = require('express');
var router = express.Router();

var users = require('../models/user');
var books = require('../models/book');
var codes = require('../models/code');
var toplist = require('../models/toplist');

router.get('/:company', ensureAuthenticated, (req, res) => {
  // Get company's toplist
  codes.getByCompany(req.params.company, (err, toplist) => {
    if (err) throw err;
    console.log("Toplist:", toplist);
  });

  // codes.getByCompany(req.params.company, (err, toplist) => {
  //   if (err) {
  //     console.error(err);
  //     return;
  //   }
  //   // Make a new array of objects with usernames instead of the id's
  //   let usernamesAndBooks = [];
  //   // Get usernames based on id's
  //   for (let x = 0; x < toplist[0].toplist.length; x++) {
  //     let userObjHolder = toplist[0].toplist[x];
  //     users.getById(Object.keys(toplist[0].toplist[x]), (err, username) => {
  //       if (err) {
  //         console.error(err);
  //         return;
  //       }
  //       // Construct an object with usernames as keys and book counts as values
  //       let temporaryObject = {
  //         [username.username]: Object.values(toplist[0].toplist[x]),
  //       }

  //       // Push this object into an array to be used in rendering
  //       usernamesAndBooks.push(temporaryObject);
  //     });
  //   }

  //   usernamesAndBooks.sort((a, b) => {
  //     a - b
  //   });

  // Render the outcome
  res.render('toplist', {
    title: res.__('toplistHeader'),
    // toplistArray: usernamesAndBooks,
    // toplistActive: true,
    // });
  });
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}