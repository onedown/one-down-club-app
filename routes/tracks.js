var express = require('express');
var router = express.Router();

var recs = require('../models/recs');
var reccats = require('../models/reccat');

router.get('/', ensureAuthenticated, (req, res) => {
  recs.getAll((err, result) => {
    if (err) throw err;
    sortLowToHigh = (a, b) => {
      if (a.index < b.index)
        return -1;
      if (a.index > b.index)
        return 1;
      return 0;
    }
    result.sort(sortLowToHigh);

    let nonFictionRay = [],
      investingRay = [],
      companyRay = [],
      marketingRay = [],
      salesRay = [];
    for (let x = 0; x < result.length; x++) {
      if (result[x].category == "nonFiction") {
        nonFictionRay.push(result[x]);
      } else if (result[x].category == "investing") {
        investingRay.push(result[x]);
      } else if (result[x].category == "companyCulture") {
        companyRay.push(result[x]);
      } else if (result[x].category == "marketing") {
        marketingRay.push(result[x]);
      } else if (result[x].category == "sales") {
        salesRay.push(result[x]);
      }
    }

    reccats.getAll((err, reccats) => {
      if (err) throw err;
      res.render('tracks', {
        title: res.__('tracksHeader'),
        tracksActive: true,
        nonFictionRay,
        investingRay,
        companyRay,
        marketingRay,
        salesRay,
        reccats
      });
    });
  });
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}