var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var users = require('../models/user');
var accessCode = require('../models/code');
var books = require('../models/book');
var mailService = require('../util/mail');

// Register
router.get('/register', function (req, res) {
	res.render('register', {
		registerActive: true,
		title: res.__('registerHeader'),
	});
});



// Login
router.get('/login', function (req, res) {
	res.render('login', {
		loginActive: true,
		title: res.__('loginHeader'),
		layout: "login"
	});
});

// Front
router.get('/front', function (req, res) {
	res.render('front');
});

// Add books
router.get('/add', ensureAuthenticated, function (req, res) {
	users.getByIdForSerialization(req.user.id, (err, result) => {
		if (err) throw err;
		console.log("RESULT", result.groups);

		res.render('add', {
			title: res.__('addBooksHeader'),
			layout: "addlayout",
			groups: result.groups,
			addActive: true,
		});
	});
});

// Password recovery
router.get('/recover', (req, res) => {
	res.render('recover', {
		title: res.__('recoverHeader'),
		recoverActive: true,
	});
});

function makeid() {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (var i = 0; i < 8; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

router.post('/recover', (req, res) => {
	users.getByEmail(req.body.recoverMe, (err, result) => {
		if (err) throw err;
		if (result.length < 1) {
			req.flash('error_msg', 'Email not found');
			res.redirect('back');
		} else {
			let updateId = {
				_id: result[0].id
			}
			let pwTemp = makeid();
			let updateTemplate = {
				password: pwTemp
			}

			users.setPassword(updateId, updateTemplate, (err, updatedUser) => {
				if (err) throw err;
				console.log(updatedUser);
				// Send mail
				mailService.mailerBoi(req.body.recoverMe, "Your password has been changed", "Your password was changed. <br> Your new password is: " + pwTemp + "<br>Login at https://app.onedown.club/users/login");

				req.flash('success_msg', res.__('passwordChanged'));
				res.render('login', {
					message: res.__('passwordChanged'),
				});
			});
		}
	})
});

// Bookshelf
router.get('/bookshelf', ensureAuthenticated, (req, res) => {
	res.render('bookshelf', {
		layout: "bookshelf",
		title: req.user.username + "'s bookshelf",
	});
});

// Single book
router.get('/books/:bookId', ensureAuthenticated, (req, res) => {
	books.getById(req.params.bookId, function (err, book) {
		if (err) {
			res.render('error', {
				title: res.__('errorHeader'),
				errorRead: res.__('errorNotFound') + "🤔"
			});
			return;
		} else if (book[0] == undefined) {
			res.render('error', {
				title: res.__('errorHeader'),
				errorRead: res.__('errorNotFound') + "🤔"
			});
		} else {
			// res.send(book);
			let myBook = null;


			if (book[0].owner == req.user.username) {
				myBook = true;
			}

			// Get userinfo for comments
			let userInfoArray = [];
			for (let x = 0; x < book[0].comments.length; x++) {
				users.getById(book[0].comments[x].userId, (err, result) => {
					if (err) throw err;
					userInfoArray.push(result);
				});
			}
			users.getById(book[0].ownerId, (err, user) => {
				if (err) throw err;
				console.log("Owner img", user);
				res.render('singlebook', {
					layout: "bookshelf",
					bookTitle: book[0].title,
					bookAuthor: book[0].author,
					bookTimeStamp: book[0].createdAt,
					thoughts: book[0].thoughts,
					owner: book[0].owner,
					image: book[0].image,
					bookId: book[0].id,
					rating: book[0].rating,
					myBook,
					comments: book[0].comments,
					ownerImg: user[0].pic,
					date: book[0].date
				});
			});
		}
	});
});


// Register User
router.post('/register', (req, res) => {
	var email = req.body.email;
	var username = req.body.username;
	var password = req.body.password;
	var accessCodeForm = req.body.accessCodeForm;


	// Validation
	req.checkBody('email', 'Email is required').notEmpty();
	req.checkBody('email', 'Email is not valid').isEmail();
	req.checkBody('username', 'Username is required').notEmpty();
	req.checkBody('password', 'Password is required').notEmpty();
	req.checkBody('accessCodeForm', 'Code is required').notEmpty();

	var errors = req.validationErrors();

	if (errors) {
		res.render('register', {
			errors: errors
		});
	} else {
		// checking if email and username are already taken
		// some of the validation is broken
		accessCode.findOne({
			accessCode: {
				$regex: accessCodeForm
			}
		}).then(doc => {
			if (doc) {
				users.findOne({
					username: {
						"$regex": "^" + username + "\\b",
						"$options": "i"
					}
				}, (err, user) => {
					users.findOne({
						email: {
							"$regex": "^" + email + "\\b",
							"$options": "i"
						}
					}, (err, mail) => {
						if (user || mail) {
							res.render('register', {
								user: user,
								mail: mail
							});
						} else {
							let companyLogoHolder = '/images/defaultpic.png';
							var newUser = new users({
								email: email,
								emailSettings: {
									onGroupPost: true,
									onGroupPick: false,
									onNewComment: true,
								},
								username: username,
								password: password,
								company: doc.company,
								companyId: doc._id,
								admin: false,
								moderator: false,
								pic: '/images/defaultpic.png',
								companyLogo: companyLogoHolder,
								emailSettings: {
									onGroupPost: true,
									onGroupPick: false,
									onNewComment: true,
								}
							});
							users.create(newUser, (err, user) => {
								if (err) throw err;
								console.log(user);
							});
							req.flash('success_msg', 'You are registered and can now login');
							res.redirect('/users/login');
						}
					});
				});
			}

		}).catch(err => {
			console.error(err);
		});

		return;
	}
});

passport.use(new LocalStrategy(
	(username, password, done) => {
		users.getByUsername(username, (err, user) => {
			if (err) throw err;
			if (!user) {
				return done(null, false, {
					message: 'Unknown user',
				});
			}

			users.comparePassword(password, user.password, (err, isMatch) => {
				if (err) throw err;
				if (isMatch) {
					return done(null, user);
				} else {
					return done(null, false, {
						message: 'Invalid password'
					});
				}
			});
		});
	}));


passport.serializeUser((user, done) => {
	done(null, user.id);
});

passport.deserializeUser((id, done) => {
	users.getByIdForSerialization(id, (err, user) => {
		accessCode.getByCompany(user.company, (err, result) => {
			if (err) throw err;
			user.companyLogo = result[0].logo;
			done(err, user);
		});
	});
});

router.post('/login',
	passport.authenticate('local', {
		successRedirect: '/',
		failureRedirect: '/users/login',
		failureFlash: true
	}),
	(req, res) => {
		res.redirect('/');
	});

router.get('/logout', (req, res) => {
	req.logout();

	req.flash('success_msg', 'You are logged out');

	res.redirect('/users/login');
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	} else {
		//req.flash('error_msg','You are not logged in');
		res.redirect('/users/login');
	}
}