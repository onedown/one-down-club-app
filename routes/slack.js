var express = require('express');
var router = express.Router();
var fetch = require('node-fetch');
var querystring = require('querystring');

var groups = require('../models/group');
var books = require('../models/book');
var users = require('../models/user');

router.get('/adduser', ensureAuthenticated, function (req, res) {
	if ('code' in req.query) {
		let uid = req.user.id;

		let clientSecret = "4b3dd36d439277d28268976b65a25057";
		let clientId = "404843339383.477743643077";

		let oauthUrl = "https://slack.com/api/oauth.access?code=" + req.query.code +
				 "&client_id=" + clientId +
				 "&client_secret=" + clientSecret +
				 "&redirect_uri=https://sk-slack-dom.localtunnel.me/slack/adduser";

		// Slack suggests putting these params in the body, but when I used that it didn't work
		// TODO: switch to passing client_id & client_secret through body
		let body = {
			client_id: clientId,
			client_secret: clientSecret,
			redirect_uri: 'https://sk-slack-dom.localtunnel.me/slack/adduser'
		}

		console.log(oauthUrl);
		fetch(oauthUrl, {
	        method: 'post',
	        body:    JSON.stringify(body),
	        headers: { 'Content-Type': 'application/json' },
	    })
	    .then(res => res.json())
	    .then(json => {
			let url = 'https://slack.com/api/users.identity?token=' + json.access_token;
			 fetch(url)
			    .then(response => response.json())
			    .then(newjson => {
			    		let slackInfo = {
      						slackUser: newjson.user.name,
      						slackId: newjson.user.id
    					}
			    		users.setSlack(uid, slackInfo, (err, uData) => {
					      if (err) throw err;
					      console.log(uData);
			    		});
			    	})
			    .catch(error => {
			      console.error(error);
			    });
		})
		.catch(error => {
			console.error(error);
		});
		
		res.render('settings', {
		    message: 'Slack user set!'
		});

	} else {
		res.end("you shouldn't be here");
	}
});

router.get('/addchannel', ensureAuthenticated, function (req, res) {
	res.render('slackbot');
});

router.post('/myshelf', function(req, res) {
	const slackReqObj = req.body;
	var slack_uid = slackReqObj.user_id;
	var slack_user = slackReqObj.user_name;
	users.getBySlackId(slack_uid, (err, uname) => {
		if (err) throw err
		if (uname == null) {
			res.end("Your slack user isn't registered, please add it from the website.");
		} else {
			books.getByUsername(uname.username, (err, shelf) => {
				if (err) throw err
				res.write("Here are the books you've reviewed:\n")
				for (var b of shelf) {
					res.write("\t" + b.title + " - " + b.rating + "\n");
				}
				res.end("");
			});
		}	
	});
});

router.post('/mygroups', function(req, res) {
	const slackReqObj = req.body;
	var slack_uid = slackReqObj.user_id;
	var slack_user = slackReqObj.user_name;
	users.getBySlackId(slack_uid, (err, user) => {
		if (err) throw err
		if (user == null) {
			res.end("Your slack user isn't registered, please add it from the website.");
		} else {
			res.write("You are in the following groups:\n")
			for (var g of user.groups) {
				res.write("\t-" + g.name + "\n");
			}
			res.end("");
		}
	})
})

router.post('/lookup', function(req, res) {

})

router.post('/getbook', function(req, res) {
	const slackReqObj = req.body;
	//For these, I may want to set it up to use slack_uid, as they're planning to change it. 
	// username is much easier, though, as it lets the users set up their own link to a slack account.
	var slack_uid = slackReqObj.user_id;
	var slack_user = slackReqObj.user_name;
	users.getBySlackId(slack_user, (err, user) => {
		if (err) throw err
		if (user == null) {
			res.end("Your username isn't registered, please add it from the website.");
		} else {

		}
	});
})

router.get('/addGroupNotify/:groupId',/** ensureAuthenticated,*/ function(req, res) {
	console.log("calling notify for group: " + req.params.groupId);
	groups.getById(req.params.groupId, (err, group) => {
		if (err) throw err
		console.log("group: \n" + group)
		for (user of group.members) {
			users.getById(user.id, (err, u) => {
				console.log("found user: \n" + u)
				if (u[0].slackId) {
					remind(u[0].slackId, "Someone added a new book!\nCheck it out at: <https://one-down-app.herokuapp.com/group/" + req.params.groupId + ">", res);
				} else {
					console.log("err: " + u[0])
				}
			})
		}
	})
})

router.get('/bookCommentNotify/:bookId',/** ensureAuthenticated,*/ function(req, res) {
	books.getById(req.params.bookId, (err, book) => {
		if (err) throw err
		if (book == null) {
			res.end("no such book");
			return;
		}
		users.getById(book[0].ownerId, (err, results) => {
			if (err) throw err
			if (results == null) {
				res.end("No such owner: " + book.ownerId)
				return;
			}
			let user = results[0];
			if (user.slackId) {
				remind(user.slackId, "Someone commented on your review!\nCheck it out <https://one-down-app.herokuapp.com/users/books/" + req.params.bookId + ">", res);
			}
		})
	})
})

module.exports = router;

function remind(slackId, msg, res) {
	let qMsg = querystring.escape(msg);
	const API_URL = "https://slack.com/api/reminders.add";
 	const apiKey = "xoxp-404843339383-467111062740-479163100672-d5d289065ee3263ab6276e23ea93f740";
 	let ts = Math.round((new Date()).getTime() / 1000);

 	var getURL = API_URL + "?token=" + apiKey + "&text=" + qMsg +
 			"&time=in%201%20seconds&user=" + slackId

 	console.log(getURL);
 	 fetch(getURL)
    .then(response => console.log("Reminder queued.\n" + response))
    .catch(error => {
      console.error(error);
    });
    res.end("done");
}

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}