var express = require('express');
var router = express.Router();
var formidable = require('formidable'),
  http = require('http'),
  util = require('util');

var groups = require('../models/group');
var users = require('../models/user');
const upload = require('../util/multer');

router.post('/', ensureAuthenticated, ensureModerator, (req, res) => {

  let singleUpload = upload.single('groupPic');
  singleUpload(req, res, function (err, some) {
    if (err) {
      return res.status(422).send({
        errors: [{
          title: 'Image Upload Error',
          detail: err.message
        }]
      });
    }

    let newGroupObj = new groups({
      name: req.body.groupname,
      members: [{
        id: req.user.id,
        name: req.user.username,
        joinedAt: Date.now(),
        profilePic: req.user.pic,
      }],
      owners: [{
        id: req.user.id,
        name: req.user.username,
        timestamp: Date.now(),
        profilePic: req.user.pic,
      }],
      groupImage: req.file ? req.file.location : '../images/group.png',
      createdAt: Date.now(),
      company: req.user.company,
      type: req.body.type,
      intro: req.body.groupdesc,
    });
    // Fix

    groups.create(newGroupObj, (err, group) => {
      if (err) throw err;
      console.log(group);

      let memberIdUse = {
        _id: req.user.id,
      }

      let addToMember = {
        $push: {
          groups: {
            id: group.id,
            name: group.name,
            pic: group.groupImage,
          }
        }
      }

      users.addGroup(memberIdUse, addToMember, (err, updatedGroup) => {
        if (err) throw err;
        console.log(updatedGroup);
      });
    });
    req.flash('success_msg', 'New group created!');
    res.render('index');
  });
});

router.get('/', ensureAuthenticated, ensureModerator, (req, res) => {
  res.render('groupcreate', {
    groupActive: true,
  });
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}

function ensureModerator(req, res, next) {
  if (req.user.moderator) {
    return next();
  } else {
    req.flash('error_msg', 'You are not a moderator :(');
    res.redirect('/');
  }
}