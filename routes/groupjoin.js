var express = require('express');
var router = express.Router();

var groups = require('../models/group');
var users = require('../models/user');

router.post('/:groupId', ensureAuthenticated, (req, res) => {

  let idUse = {
    _id: req.params.groupId
  }

  let addToGroup = {
    $push: {
      members: {
        id: req.user.id,
        name: req.user.username,
        profilePic: req.user.pic,
        joinedAt: Date.now(),
      }
    }
  }

  let memberIdUse = {
    _id: req.user.id,
  }

  let addToMember = {
    $push: {
      groups: {
        id: req.params.groupId,
        name: req.body.groupname,
      }
    }
  }

  groups.addUser(idUse, addToGroup, (err, updatedGroup) => {
    if (err) throw err;
    users.addGroup(memberIdUse, addToMember, (err, updatedGroup) => {
      if (err) throw err;
      res.redirect('back');
      req.flash('success_msg', 'Joined to group!');
    });
  });
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}