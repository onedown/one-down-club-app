var express = require('express');
var router = express.Router();


var users = require('../models/user');
var books = require('../models/book');

router.get('/stats', ensureAuthenticated, ensureModerator, (req, res) => {


  users.getByCompany(req.user.companyId, (err, result) => {
    if (err) throw err;

    let bookResults = [];
    let totalBooks = 0;
    let checker = 0;
    let avgTotal = 0;

    for (let x = 0; x < result.length; x++) {
      books.getByOwnerId(result[x].id, (err, found) => {
        if (err) throw err;
        // Calculate words function
        function count_words(str1) {
          //exclude  start and end white-space
          str1 = str1.replace(/(^\s*)|(\s*$)/gi, "");
          //convert 2 or more spaces to 1  
          str1 = str1.replace(/[ ]{2,}/gi, " ");
          // exclude newline with a start spacing  
          str1 = str1.replace(/\n /, "\n");
          return str1.split(' ').length;
        }
        // Compose review average lengths array
        let avgHolder = 0;
        // Sum up char count of every review
        found.forEach(element => {
          avgHolder += count_words(element.thoughts);
        });
        // Calculate the avg
        avgHolder = avgHolder / found.length;
        // Compose bookResults array and count total books and avg
        let object = {};
        object["id"] = result[x].id;
        object["name"] = result[x].username;
        object["count"] = found.length;
        object["avg"] = avgHolder ? Math.round(avgHolder) : "-";
        object["groups"] = result[x].groups.length;
        avgTotal += avgHolder ? avgHolder : 0;
        bookResults[x] = object;
        totalBooks += found.length;
        checker++;
        logger();
      });
    }

    function logger() {
      if (checker === result.length) {
        // Calculate the total avg
        avgTotal = avgTotal / result.length;
        avgTotal = Math.round(avgTotal);
        // Render page
        res.render('stats', {
          stats: result[0],
          userTotal: result.length,
          modStatsActive: true,
          title: "Statistics for " + (result[0] ? result[0].company : "no-one"),
          bookResults,
          totalBooks,
          avgTotal,
        });
      }
    }
  });
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}

function ensureModerator(req, res, next) {
  if (req.user.moderator) {
    return next();
  } else {
    req.flash('error_msg', 'You are not a moderator :(');
    res.redirect('/');
  }
}