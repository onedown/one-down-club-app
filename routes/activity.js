var express = require('express');
var router = express.Router();

var groups = require('../models/group');

router.post('/', ensureAuthenticated, (req, res) => {
  let newActivity = {
    $push: {
      entries: {
        user: req.body.user,
        data: req.body.data,
        timestamp: Date.now(),
      }
    }
  }
  groups.addActivity(req.body.groupId, newActivity, (err, addedActivity) => {
    if (err) throw err;
    console.log(addedActivity);
  });
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    //req.flash('error_msg','You are not logged in');
    res.redirect('/users/login');
  }
}