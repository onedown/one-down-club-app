var express = require('express');
var router = express.Router();

var users = require('../models/user');
var books = require('../models/book');

router.get('/', ensureAuthenticated, ensureAdmin, (req, res) => {
  users.getAll((err, users) => {
    res.render('manage', {
      users,
      manageActive: true,
    })
  });
});

router.post('/demote/:id', ensureAuthenticated, ensureAdmin, (req, res) => {
  let idUse = {
    _id: req.params.id
  }

  let permsUse = {
    moderator: false
  }

  users.setPermissions(idUse, permsUse, (err, updated) => {
    if (err) throw err;
    req.flash('success_msg', 'User demoted');
    res.redirect('back');
  });
});

router.post('/promote/:id', ensureAuthenticated, ensureAdmin, (req, res) => {
  let idUse = {
    _id: req.params.id
  }

  let permsUse = {
    moderator: true
  }
  users.setPermissions(idUse, permsUse, (err, updated) => {
    if (err) throw err;
    req.flash('success_msg', 'User promoted');
    res.redirect('back');
  });
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    //req.flash('error_msg','You are not logged in');
    res.redirect('/users/login');
  }
}

function ensureAdmin(req, res, next) {
  if (req.user.admin) {
    return next();
  } else {
    req.flash('error_msg', 'You are not an admin :(');
    res.redirect('/');
  }
}