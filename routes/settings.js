var express = require('express');
var router = express.Router();
var User = require('../models/user');
var i18n = require("i18n");


const upload = require('../util/multer');

var users = require('../models/user');

router.get('/', ensureAuthenticated, function (req, res) {
  users.getById(req.user.id, (err, result) => {
    if (err) throw err;
    res.render('settings', {
      title: res.__('settingsHeader'),
      settingsActive: true,
      emailSettings: result.emailSettings,
    });
  });
});

router.post('/', ensureAuthenticated, (req, res) => {
  const singleUpload = upload.single('profilePic');
  singleUpload(req, res, function (err, some) {
    if (err) {
      return res.status(422).send({
        errors: [{
          title: 'Image Upload Error',
          detail: err.message
        }]
      });
    }

    if (req.file) {
      req.flash('success_msg', 'Profile pic changed!')
      res.render('settings');

      let usePic = {
        pic: req.file.location
      }
      User.setPic(req.user.id, usePic, (err, updated) => {
        if (err) console.error(err);
      });
    } else {
      req.flash('error_msg', 'No file selected!');
      res.redirect('back');
    }
  });
});


router.post('/email', ensureAuthenticated, (req, res) => {
  var errors = req.validationErrors();

  if (errors) {
    res.render('settings', {
      error: errors
    });
  } else {

    let email = req.body.newMail;

    users.findOne({
      email: {
        "$regex": "^" + email + "\\b",
        "$options": "i"
      }
    }, (err, mail) => {
      if (err) throw err;
      if (mail) {
        res.render('settings', {
          email: mail,
        });
      } else {
        let newUser = {
          email: email,
        }
        User.setEmail(req.user.id, newUser, (err, updatedUser) => {
          if (err) throw err;
          res.render('settings', {
            message: 'Email changed!'
          });
        });
      }
    });
  }
});

router.post('/password', ensureAuthenticated, (req, res) => {
  let updateTemplate = {
    password: req.body.newPassword,
  }

  let updateId = {
    _id: req.user.id,
  }

  User.setPassword(updateId, updateTemplate, (err, updatedUser) => {
    if (err) throw err;
    console.log(updatedUser);
    res.render('settings', {
      message: 'Password changed!'
    });
  });
});

router.post('/lang', ensureAuthenticated, (req, res) => {
  res.cookie('lang', req.body.lang);
  i18n.setLocale(i18n, req.body.lang);
  res.redirect(req.headers.referer);
});

router.post('/notification', ensureAuthenticated, (req, res) => {
  // if (req.body.settingName == "onGroupPost") {
  //   let newSettings = {
  //     $push: {
  //       emailSettings: {
  //         onGroupPost: req.body.setting,
  //       }
  //     }
  //   }
  //   User.setNotificationSettings(req.user.id, newSettings, (err, updated) => {
  //     if (err) throw err;
  //     console.log(updated);
  //   });
  // } else if (req.body.settingName == "onGroupPick") {
  //   let newSettings = {
  //     $push: {
  //       emailSettings: {
  //         onGroupPick: req.body.setting,
  //       }
  //     }
  //   }
  //   User.setNotificationSettings(req.user.id, newSettings, (err, updated) => {
  //     if (err) throw err;
  //     console.log(updated);
  //   });
  // } else if (req.body.settingName == "onNewComment") {
  //   let newSettings = {
  //     $push: {
  //       emailSettings: {
  //         onNewComment: req.body.setting,
  //       }
  //     }
  //   }
  //   User.setNotificationSettings(req.user.id, newSettings, (err, updated) => {
  //     if (err) throw err;
  //     console.log(updated);
  //   });
  // } else {
  //   req.flash('error_msg', 'Invalid settings!');
  //   res.redirect('/settings');
  // }


});

module.exports = router;

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}