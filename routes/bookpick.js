var express = require('express');
var router = express.Router();
var fetch = require('node-fetch');

var groups = require('../models/group');
const upload = require('../util/multer');

router.get('/', ensureAuthenticated, (req, res) => {
  res.render('');
});

router.post('/', ensureAuthenticated, (req, res) => {

  const singleUpload = upload.single('manualBookPic');
  singleUpload(req, res, function (err, some) {
    if (err) {
      return res.status(422).send({
        errors: [{
          title: 'Image Upload Error',
          detail: err.message
        }]
      });
    }

    if (req.file) {
      let userToUpdate = {
        '$set': {
          'members.$.pick': {
            ownerId: req.user.id,
            ownerName: req.user.username,
            bookTitle: req.body.titleField,
            bookAuthor: req.body.authorField,
            bookImage: req.file.location,
            bookISBN: req.body.isbn ? req.body.isbn : null,
          }
        }
      }

      // //Send slack notification
      // const query_url = "http://sk-slack-dom.localtunnel.me/slack/addGroupNotify/" + req.body.groupId
      // console.log(query_url);
      // fetch(query_url)
      //   .then(response => console.log("fetch res: " + response))
      //   .then(res.redirect('back'))
      //   .catch(error => {
      //     console.error(error);
      //   });

      // does this exist yet? seems like you want to add a pick to a user, not sure you need groups table
      // It now does :D

      let idUse = {
        _id: req.body.groupIdForPicker,
        'members.id': req.user.id
      }
      groups.addBookPick(idUse, userToUpdate, function (err, updatedUser) {
        if (err) throw err;
        req.flash('success_msg', 'Pick updated!');
        res.redirect('back');
        console.log("UPDATED", updatedUser);
      });

    } else if (req.body.imageField) {
      let userToUpdate = {
        '$set': {
          'members.$.pick': {
            ownerId: req.user.id,
            ownerName: req.user.username,
            bookTitle: req.body.titleField,
            bookAuthor: req.body.authorField,
            bookImage: req.body.imageField,
            bookISBN: req.body.isbn ? req.body.isbn : null,
          }
        }
      }

      // //Send slack notification
      // const query_url = "http://sk-slack-dom.localtunnel.me/slack/addGroupNotify/" + req.body.groupId
      // console.log(query_url);
      // fetch(query_url)
      //   .then(response => console.log("fetch res: " + response))
      //   .then(res.redirect('back'))
      //   .catch(error => {
      //     console.error(error);
      //   });

      // does this exist yet? seems like you want to add a pick to a user, not sure you need groups table
      // It now does :D

      let idUse = {
        _id: req.body.groupIdForPicker,
        'members.id': req.user.id
      }
      groups.addBookPick(idUse, userToUpdate, function (err, updatedUser) {
        if (err) throw err;
        req.flash('success_msg', 'Pick updated!');
        res.redirect('back');
        console.log("UPDATED", updatedUser);
      });
    } else {
      req.flash('error_msg', 'No file selected!');
      res.redirect('back');
    }
  });
});

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}

module.exports = router;