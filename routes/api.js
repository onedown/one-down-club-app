var express = require('express');
var router = express.Router();

var groups = require('../models/group');
var books = require('../models/book');
var users = require('../models/user');


router.get('/groups', function (req, res) {
	if ('groupId' in req.query) {
		groups.getById(req.query.groupId, (err, group) => {
			if (err) throw err
			goodResult(group, res);
		});
	} else {
		badRequest(res);
	}
});

router.get('/books', function (req, res) {
	if ('bookId' in req.query) {
		books.getById(req.query.bookId, (err, book) => {
			if (err) throw err
			goodResult(book, res);
		})
	} else {
		badRequest(res);
	}
})

router.get('/users', function (req, res) {
	if ('username' in req.query) {
		users.getByUsername(req.query.username, (err, user) => {
			if (err) throw err
			goodResult(user, res);
		});
	} else {
		badRequest(res);
	}
});

function goodResult(data, res) {
	if (data != null) {
		res.end(JSON.stringify(data));
	} else {
		badRequest(res);
	}
}

function badRequest(res) {
	res.sendStatus(400);
}

module.exports = router;