var express = require('express');
var router = express.Router();

var groups = require('../models/group');

router.post('/', ensureAuthenticated, (req, res) => {
  console.log("Userid", req.body.userid);

  let removeUserUse = {
    $pull: {
      selectBooks: {
        id: req.user.id
      }
    }
  }

  let removeGroupUse = {
    _id: req.body.groupid
  }

  groups.addUser(removeGroupUse, removeUserUse, (err, user) => {
    if (err) throw err;
    console.log(user);
  });
  res.redirect('back');
  req.flash('success_msg', 'Pick removed');
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}