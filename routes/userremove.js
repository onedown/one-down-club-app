var express = require('express');
var router = express.Router();

var group = require('../models/group');
var users = require('../models/user');

router.post('/', ensureAuthenticated, ensureAdmin, (req, res) => {
  let userUse = {
    $pull: {
      members: {
        id: req.body.userToRemove,
      }
    }
  }
  console.log("User to remove:", req.body.userToRemove);

  console.log("Group to remove from:", req.body.removeGroupId);

  // Why the fuck does req.params.Id work??
  group.addUserToGroup(req.params.Id, userUse, (err, updatedGroup) => {
    if (err) throw err;
    console.log(updatedGroup);
  });

  let removeUserUse = {
    $pullAll: {
      groups: {
        id: req.body.removeGroupId
      }
    }
  }

  users.addGroup(req.params.Id, removeUserUse, (err, updatedUser) => {
    if (err) throw err;
    console.log(updatedUser);
  });

  res.redirect('back');
  req.flash('success_msg', 'Joined to group!');
});

router.get('/', ensureAuthenticated, ensureAdmin, (req, res) => {
  req.flash('success_msg', 'Removed');
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}


function ensureAdmin(req, res, next) {
  if (req.user.admin) {
    return next();
  } else {
    req.flash('error_msg', 'You are not an admin :(');
    res.redirect('/');
  }
}