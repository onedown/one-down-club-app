var express = require('express');
var router = express.Router();
var schedule = require('node-schedule');

var groups = require('../models/group');
var users = require('../models/user');
var activity = require('../models/activity');

const upload = require('../util/multer');

// Schedule emptying every group's picks in the turn of the month

var j = schedule.scheduleJob('0 0 0 1 * *', function () {
  groups.deletePicks((err, updated) => {
    if (err) throw err;
    console.log(updated);
  });
});

// router.get('/test', (req, res) => {
//   groups.deletePicks((err, updated) => {
//     if (err) throw err;
//     console.log(updated);
//   });
// });

router.get('/:groupId', ensureAuthenticated, (req, res) => {
  groups.getById(req.params.groupId, (err, group) => {
    if (err) throw err;

    let belongsToGroup = null;
    let myPick = null;
    let pickMembers = group.members;
    let originalMembers = group.members;
    let roundCounter = 0;



    console.log(originalMembers.length);

    // originalMembers.forEach(member => {

    if (originalMembers.length >= 1) {
      for (let x = 0; x < originalMembers.length; x++) {

        // Query each member's profile pics
        users.getById(originalMembers[x].id, (err, result) => {
          if (err) throw err;
          if (result[0] && originalMembers[x]) {

            originalMembers[x].profilepic = result[0].pic;
          } else if (originalMembers[x]) {
            originalMembers[x].profilepic = "https://upload.wikimedia.org/wikipedia/commons/7/72/Default-welcomer.png"
          }
        });

        // Check if this user is the one loading the page
        // Check if they belong to the group and determine their pick if any
        if (originalMembers[x].id === req.user.id) {
          belongsToGroup = true;
          if (originalMembers[x].pick) {
            myPick = {
              user: req.user.username,
              id: req.user.id,
              bookTitle: originalMembers[x].pick.bookTitle,
              bookAuthor: originalMembers[x].pick.bookAuthor,
              bookImage: originalMembers[x].pick.bookImage,
            }
          }
          pickMembers[x] = null;
        }

        roundCounter++;
        continuationCheck();
      }
    } else {
      res.render('group', {
        title: res.__('groupHeader') + group.name,
        layout: "group",
        group: group,
        pickMembers,
        myPick,
        currentUserId: req.user.id,
        currentUserName: req.user.username,
        currentUserProfilePic: req.user.pic,
        belongsToGroup: false,
        isEmpty: true,
      });
    }

    function continuationCheck() {
      if (roundCounter === originalMembers.length) {
        let isowner = null;
        let owners = group.owners;
        owners.forEach(owner => {
          if (owner.id === req.user.id) {
            isowner = true;
          }
        });

        // Get activity
        activity.getByGroup(req.params.groupId, (err, activities) => {
          if (err) throw err;
          activites = activities.reverse();

          res.render('group', {
            title: res.__('groupHeader') + group.name,
            layout: "group",
            group: group,
            pickMembers,
            myPick,
            isowner,
            currentUserId: req.user.id,
            currentUserName: req.user.username,
            currentUserProfilePic: req.user.pic,
            belongsToGroup,
            activities,
          });
        });
      }
    }
  });
});

// Listgroups

router.get('/for/:companyId', ensureAuthenticated, (req, res) => {
  groups.getByCompany(req.params.companyId, (err, groups) => {
    if (err) throw err;
    for (let b = 0; b < groups.length; b++) {
      for (let f = 0; f < groups[b].members.length; f++) {
        if (groups[b].members[f]) {
          if (groups[b].members[f].id === req.user.id) {
            groups[b].isMember = true;
          }
        }
      }
    }
    res.render('listgroups', {
      title: res.__('allGroupsHeader'),
      groups,
      groupsActive: true,
    })
  })
});

// Delete group

router.post('/delete/:groupId', ensureAuthenticated, (req, res) => {
  groups.getById(req.params.groupId, (err, found) => {
    if (err) throw err;
    let counter = 0;
    let members = found.members;
    members.forEach(member => {
      let removeUserUse = {
        $pull: {
          groups: {
            id: req.params.groupId
          }
        }
      }

      let userIdToChange = {
        _id: member.id,
      }

      users.addGroup(userIdToChange, removeUserUse, (err, updatedUser) => {
        if (err) throw err;
        console.log(updatedUser);
        counter++;
        destroy();
      });
    });

    if (members.length === 0) {
      destroy();
    }

    function destroy() {
      if (counter === members.length) {
        groups.destroy(req.params.groupId, (err, deleted) => {
          if (err) throw err;
          console.log(deleted);
          req.flash('success_msg', 'Group deleted!');
          res.redirect('back');
        });
      }
    }
  });
});

router.get('/delete/:groupId', ensureAuthenticated, (req, res) => {
  groups.getAll((err, groups) => {
    if (err) throw err;
    users.
    res.render('groupdelete', {
      groups
    });
  });
});

// Change group cover

router.post('/cover', ensureAuthenticated, (req, res) => {

  const singleUpload = upload.single('groupPic');
  singleUpload(req, res, function (err, some) {
    if (err) {
      return res.status(422).send({
        errors: [{
          title: 'Image Upload Error',
          detail: err.message
        }]
      });
    }

    if (req.file) {
      let groupPicUse = {
        groupImage: req.file.location,
      }
      groups.addPic(req.body.groupId, groupPicUse, (err, updated) => {
        if (err) {
          console.log(err);
          throw err;
        }
        req.flash('success_msg', 'Pic Changed!');
        res.redirect('back');
      });
    } else {
      res.render('error', {
        title: res.__('errorHeader'),
        errorRead: res.__('somTingWong') + " 😞"
      });
    }
  });
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}