var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var books = require('../models/book');

// ME OLLAAN JO SAATANA BOOKEDITISSÄ, EI TARVI ENÄÄ UUDESTAAN LAITTAA VITTU
router.post('/', ensureAuthenticated, (req, res) => {
  let bookId = req.body.bookId;
  let newThoughtsText = req.body.thoughts;
  let singleBook = {
    thoughts: newThoughtsText,
    updatedAt: Date.now(),
  }
  books.addThoughts(bookId, singleBook, function (err, book) {
    if (err) throw err;
    console.log("Response from book edit", book);
  });

  req.flash('success_msg', 'Book edited!');
  res.redirect('back');
});


function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}

module.exports = router;