var express = require('express');
var router = express.Router();
var i18n = require('i18n');

var accessCode = require('../models/code');
var users = require('../models/user');

var accessCode = require('../models/code');
var users = require('../models/user');

// Get Homepage
router.get('/', ensureAuthenticated, function (req, res) {
	accessCode.getByCompanyId(req.user.companyId, (err, result) => {
		console.log(result);

		if (err) throw err;
		res.render('index', {
			title: res.__('hello'),
			result,
			indexActive: true,
		});
	});
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	} else {
		req.flash('error_msg', 'Log in first!');
		res.redirect('/users/login');
	}
}