var express = require('express');
var router = express.Router();

var users = require('../models/user');

router.post('/:id', ensureAuthenticated, (req, res) => {
  let userIdToRemove = req.params.id;
  users.destroy(userIdToRemove, (err, user) => {
    if (err) throw err;
    console.log("This one was removed:", user);
  });
  res.redirect('back');
});

module.exports = router;

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'Log in first!');
    res.redirect('/users/login');
  }
}