var i18n = require('i18n');
var urls = require('./urls.json');

i18n.configure({
  // setup some locales - other locales default to en silently
  locales: ['en', 'fi'],
  // where to store json files - defaults to './locales' relative to modules directory
  directory: __dirname + '/locales',
  defaultLocale: 'fi',
  // sets a custom cookie name to parse locale settings from  - defaults to NULL
  cookie: 'lang',
});

module.exports = function (req, res, next) {
    i18n.init(req, res);
    res.local('__', res.__);

    var current_locale = i18n.getLocale();

    return next();
  },


  module.exports = {
    //Setup our default layout
    defaultLayout: 'layout',

    //More handlebars configuration

    //Register handlebars helpers
    helpers: {
      //Register your helpers
      //Helper for multiple languages
      i18n: function () {
        return i18n.__.apply(this, arguments);
      },
      //This helper is part of i18n and it is used for multiple languages for plural and singular. Check the documentation for more details:      https://github.com/mashpie/i18n-node
      __n: function () {
        return i18n.__n.apply(this, arguments);
      },
      //Custom helper to reuse urls
      url: function (name) {
        //We only need to access to the JSON previously loaded, and   return the value of the key we receive as a parameter
        return urls[name];
      },
      ifEquals: (arg1, arg2, options) => {
        return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
      },
    }
  }