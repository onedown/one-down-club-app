# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


# [Log]


## [1.0] - 17-01-19
### #1
- Testing book add without isbn

## [1.0] - 15-01-19
### #2
- Fixed a bug where empty groups couldn't be deleted
- Changed book add search results from 10 to 5

## [1.0] - 15-01-19
### #1
- New manual add done
- Fixed profile pic query inside dbook

## [1.0] - 14-01-19
### #2
- Restructured add.js
- Started working on new add manually

## [1.0] - 14-01-19
### #1
- Small fixes for IE book- and groupview

## [1.0] - 11-01-19
### #2
- Book add search now includes our own db results (those books are excluded from bookdepo results)
- Fixed a bug where bookpicks couldn't be added from search anymore

## [1.0] - 11-01-19
### #1
- Added info popup to manual add book
- Deleting a group now removes the group from every user (removes listing from sidebar)
- Manual bookpicks added
- Edited front page message
- Discover books changed to "Company X's Library"

## [1.0] - 10-01-19
### #3
- Empty group handling added

## [1.0] - 10-01-19
### #2
- Bookpick profile pic bug FIXED

## [1.0] - 10-01-19
### #1
- Bookpick profile pic bug still persists
- Belonging to a group is now reflected properly on showing/hiding the picker

## [1.0] - 09-01-19
### #1
- Attempted fix for random profile pic problem on bookpicks
- Added a finnish isbn search help link for manual book adding

## [1.0] - 08-01-19
### #3
- Fixed problem with adding a mod/admin

## [1.0] - 08-01-19
### #3
- Started to implement the ability for users to change the dates of the submitted books

## [1.0] - 08-01-19
### #2
- Fix for group view where long group names made group profile pic skew

## [1.0] - 08-01-19
### #1
- Fix for IE front page full height

## [1.0] - 07-01-19
### #2
- Added statistics for mods about their own company

## [1.0] - 07-01-19
### #1
- Statistics about users for admins
  - Same logic can be used for moderators to use on their own company

## [1.0] - 04-01-19
### #4
- Default profile pic fixed

# [1.0] - 04-01-19
First release

## [0.2] - 04-01-19
### #3
- Changed reading tracks titles to Non-Fiction 101, Management, Sales, Personal Growth

## [0.2] - 04-01-19
### #2 Wholedelete
- Messed up, wholedelete branch now includes bookpicker styles changes
- Deleting book now deletes the book, related activities and the dbook if it is the only review under it

## [0.2] - 04-01-19
### #1
- [Lesson learned](https://stackoverflow.com/questions/15691224/mongoose-update-values-in-array-of-objects) on how to update an object on an array:
~~~~
let idUse = {
  _id: req.body.groupIdForPicker,
  'members.id': req.user.id
}

let userToUpdate = {
    '$set': {
      'members.$.pick': {
        ownerId: req.user.id,
        ...
      }
    }
~~~~
- Bookpicks are now actually working, like for real this time

## [0.2] - 03-01-19
### #2
- Long nav elements are truckated now
- Creating groups without a profile pic now works
- Fontawesome is now local

## [0.2] - 03-01-19
### #1
- Added a zero to months in date-places
- Bookpicker works, user can pick books and change their picks
- Company logos are now fetched on serialization
- Enabled reading tracks

## [0.2] - 02-01-19
### #1
- Started implementing bookpick reset in the turn of each month

## [0.2] - 28-12-18
### #1
- Prototype of bookpicks working

## [0.2] - 19-12-18
### #2
- Refined mobile styles
- Admins can now modify recommendation category descriptions

## [0.2] - 19-12-18
### #1
- Added mobile styles

## [0.2] - 18-12-18
### #1
- Recs categories now working (finally)
- The way categories work is a hack

## [0.2] - 17-12-18
### #1
- Added link to recs

## [0.2] - 14-12-18
### #1
- Learning tracks feature basic version completed

## [0.2] - 13-12-18
### #1
- Manually adding a book now requires user to add a cover photo
- Started building learning tracks feature

## [0.2] - 12-12-18
### #1
- Moderators can change group pics

## [0.2] - 11-12-18
### #2
- Book names on group activity are now limited to two rows
- Stars align
- Owner profile pic on single books

## [0.2] - 11-12-18
### #1
- Usernames inside a group are now links to their bookshelves
- Usernames in a comment on a book are links too
- Added join/leave buttons to grouplist, added member count
- Error handeling added to dbooks and books

## [0.2] - 6-12-18
### #4
- Users now get the current company logo
- Users registered by admin now have all the fields normal registered users have

## [0.2] - 6-12-18
### #3
- Added a feature for admins to see every company with their relevant information

## [0.2] - 6-12-18
### #2
- Book's thoughts now preserve line breaks and formatting

## [0.2] - 6-12-18
### #1 
- UI changes
- Group view updated
- Admins can now promote users
- Fixed dbook avg rating bug
- Also fixed activity book rating (was string, parsed to number)
- Fixed manual book add activity creation bug where book cover photos were not added

## [0.2] - 6-12-18
### #3
- Fix is not to add res.end()
- Fix is to add res.render on the right spot
- Headers are dumb

## [0.2] - 6-12-18
### #2 
- On production, file uploading resulted in socket timeout because the connection was never terminated
  * Fix is to add res.end()

## [0.2] - 6-12-18
### #1
- Merged develop

## [0.2] - 5-12-18
### #1
- Fixed "rated" translation on group + dbooks (needed to switch default lang)
- Profile pic field cannot be empty
- Disabled some settings, created a new disabled-link utility class

## [0.2] - 3-12-18
### #3 
- Disabled bookpicks for now
- Image upload with bucketeer acutally works

## [0.2] - 3-12-18
### #2 
- Fixed mongo error for depriciation warning

## [0.2] - 3-12-18
### #1 Bucketeer for dev
- Added bucketeer for development

## [0.2] - 2-12-18
### 2 More small fixes
- Changed input fields from fancy materializecss js things to more normal ones
- Fixed Dbook book link color

## [0.2] - 2-12-18
### 1 Small fixes
- Group delete removed from mods
- Book search bug fixed where results would not show if there was an image missing

## [0.2] - 29-11-18
### #2
- All arrow functions replaced with traditional functions on /js
- Newer books are now first on bookshelf
- Fixed bug where moderators were required admin priviledges to access mod settings
- Dates are now european format

## [0.2] - 29-11-18
### #1
- Quick fix for requiring the rate field
- Changed book search from fetch to xmlhttprequest
- Various other small fixes 
- Added simple password recovery

## [0.2] - 29-11-18
### #1
- Changing password now requires a confirmation

## [0.2] - 27-11-18
### #1
- Added a favicon
- Possible fix for book search

## [0.2] - 26-11-18
### #2 Image upload works
- Uploading images in profile pic / company cover/logo / group creation now WORKS

## [0.2] - 26-11-18
### #1 Activity model
- Added a model for activities
- Groups now display activities
- Moderators can change company color

## [0.2] - 23-11-18
### #2 
- Commenting

## [0.2] - 23-11-18
### #1 Email notifications
- Added email notifications
  * Import mail.js from /utils, use mailerBoi() to send mail
  * Expects recipient email, header and content (html)
- Settings now include notification settings
- User model now includes emailSettings object which has
  * onGroupPost (when someone posts to a group user is part of)
  * onGroupPick (when someone picks a group which user is part of)
  * onNewComment (when user's book gets new comment, comments do not exist yet)
- Settings now include language selection

## [0.2] - 22-11-18
### #1 
- Bookshelf books are now narrower so Tuukka can be happy
- Added a link property to dbook review objects
- Added success, error message templates
- Admin and moderator checks
- Added translations (only finnish for now)
- Acitivity model

## [0.2] - 21-11-18
### #1
- Added posting to groups
- Added menu active indicators

## [0.2] - 20-11-18
### #2 Mobile first
- Introduced mobile styles

## [0.2] - 20-11-18
### #1
- Add default admin if db is empty
- Added SASS
  * Utilized with custom colors
  * Still need to figure out how to display companys custom colors to according to the 

## [0.2] - 19-11-18
### #2 
- Admins can now add new companies

## [0.2] - 19-11-18
### #1
- Added default avatars
- Added default group pic
- Moderators can now delete groups
  * Removes group from groups, group obj from user
- UI tweaks

## [0.2] - 15-11-18
### #1 Visuality for companies
- Added logo and cover image for company
- Changed Profile Pic to Settings
  * Now includes email and pw change
- Companies now have logos and cover photos
  * Moderators can change their logos and covers
- Changing pictures doesn't work properly (headers already sent)

## [0.2] - 14-11-18
### #2 Discover books
- TO BE NOTED: old users do not have companyId field thus toplist book adds are going to be messed up
- Introducing discover books (dbooks)
  * Accessible by isbn (discover/isbn)
  * Have similar attributes as single books, with the exception of an array of review objects
  * Dbooks are company-specific

## [0.2] - 14-11-18
### #1 Toplist sorting
- Attempted to fix toplist sorting

## [0.2] - 12-11-18
### #1 Toplist logic
- Toplists backend logic is almost done, still needs sorting

## [0.2] - 8-11-18
### #2 Toplists
- Added owner id to book model
- Added toplist to code model
- Introducing toplists
  * Based on code leaderboard
- On bookadd update company's toplist (this still needs work)

## [0.2] - 8-11-18
### #1 Moderators, group stuff
- Admins can now register admins and moderators
- Modified addmanually.js, bookpick.js to only execute when their root element (eg. .addform) is found
- Admin and moderator status now shows up under username in the menu 
- Group now checks if the current user belongs to the group
- User can now leave a group

## [0.2] - 5-11-18
### #1 API route
- Added API route (not registered yet)
- Query for
 * Users
 * Groups
 * Books

## [0.2] - 1-11-18
### #1 Activities
- Page refreshes upon picking a book
- Route for adding an activity created

## [0.2] - 29-10-18
### #1 Updated group removing logic, bookpicks
- Users can now change their bookpicks
- Removing user from group now actually removes them (update both group & user)

## [0.2] - 26-10-18
### #4 Small fixes
- Own book now actually is hidden, doesn't need fixing anymore
- Own pick shows nicely
- Picks are labeled

## [0.2] - 26-10-18
### #3 Book picking done right
- Fixed group layout
- Picking doesn't suck anymore
- Own pick doenst show up
- Previous still needs fixing

## [0.2] - 26-10-18
### #2 Group creation fixed
- List of all of company's groups
- Group creation fixed
- Book add still broken

## [0.2] - 26-10-18
### #1 Bookpicks
- Stars now don't animate where they don't need to
- Only owners can remove users from group
- Show each members' picks 
- Let users pick their book
- Wipe picks when month changes
- Fixed major problem with the group data structure
- Users can only edit and remove own books

## [0.2] - 25-10-18
### #4 Adding manually, uploading fixes
- Profile pic changing page updated visually
- Picture uploading works both locally and deployed
- If pic loading breaks, blame forwardslash on path
- Adding books manually now supported

## [0.2] - 25-10-18
### #3 Image upload fixed
- Uploading images now doesn't produce H18 error
- Path structure potentially fixed

## [0.2] - 25-10-18
### #2 Image uploading modified
- Group image logic now works
- Potentially fixing the H18 error on Heroku on image uploads

## [0.2] - 25-10-18
### #1 Small fixed and adds
- Added modal amimations
 * Container disappears after all modals are animated away
- Profile pic parsing bettered
- Group creation made more complete

## [0.2] - 24-10-18
### #4 Bug fixes
- Fixed group joining
 * Add group to member correctly
- Added 404 error page
 * Needs more error handling
- No more babbys first javascript bugs in add books

## [0.2] - 24-10-18
### #3 Tweaks and admins
- Datechanger.js now changes dates according to european format
 * Works correctly on bookshelf, singlebook
- Removed company pages form users.js
- Added user property admin
- Admins can
 * Create groups
 * Register new admins
  - Needs to be updated to select company
 * Manage users
  - List
  - More to come
- User now actually joins the group
 * Group id added to user's groups array
 * In manage user's groups need front tweaking

## [0.2] - 24-10-18
### #2 Book search results
- Book search results now display correctly 
- Upon selecting item in results, correct isbn and image url values are inserted

## [0.2] - 24-10-18
### #1 Star rating
- Star rating based on [Rater.js](http://auxiliary.github.io/rater/)
 * Give book a star rating upon adding
 * Show star rating on bookshelf
 * Show star rating on singlebooks
- Basic bookshelf hover animations

## [0.2] - 22-10-18
### #3 Mobile optimization
- Added Kapa-resolution -proof styles

## [0.2] - 22-10-18
### #2 Additional styles
- Finished styling for judgement day

## [0.2] - 22-10-18
### #1 New styles
- Added actual styles, starting to slowly resemble a product
- Added intro to group schema
- Needs heavy optimization, error handling

## [0.2] - 21-10-18
## First release, whoop!
- (Somewhat)Working features:
  * Profiles
  * Registering (needs company code)
  * Add books to bookshelf (Bookdepository API)
  * Remove books from bookshelf
  * Edit books in bookshelf
  * Create groups
  * Join groups
  * Groups display their owners, members, member book picks
- Nice work Lauri

## [0.1] - 21-10-18
### #4 Deleting books
- Added the option to delete books from bookshelf

## [0.1] - 21-10-18
### #3 Merged feature-groups
- Merged branch feature-groups back to develop

## [0.1] - 21-10-18
### #2 Groups work
- Note: routes groupjoin and userremove req.params.Id works in a mysterious way

## [0.1] - 21-10-18
### #1 Added
- Initial commit