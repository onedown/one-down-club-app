var webdriver = require('selenium-webdriver');

var driver = new webdriver.Builder().forBrowser('internet explorer').build();
var url_base = "https://test.onedown.club";

var user = 'testi1';
var pass = '123';

//Login code here, then each test case is in a function- run as a group using the 'tests' array
driver.get(url_base);
driver.findElement(webdriver.By.name('username')).sendKeys(user);
driver.findElement(webdriver.By.name('password')).sendKeys(pass, webdriver.Key.RETURN);
driver.sleep(1000);

function checkLogin() {
	return driver.findElements(webdriver.By.className('nav-element')).then(function (links) {
		if (links.length < 4) {
			return "checkLogin: fail";
		} else {
			return "checkLogin: pass";
		}
	})
}

function randomID() {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (var i = 0; i < 5; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

function changeEmail() {
	return driver.get(url_base + "/settings").then(function () {
		var emailID = randomID() + "@test.net";
		return driver.findElement(webdriver.By.name('newMail')).sendKeys(emailID, webdriver.Key.RETURN).then(function () {
			return driver.findElements(webdriver.By.className('content-box-content')).then(function (boxes) {
				return boxes[1].getText().then(function (res) {
					return res.includes(emailID) ? "changeEmail: pass" : "changeEmail: fail";
				})
			})
		})
	})
}

function checkBookshelf() {
	return driver.get(url_base + "/userBooks/" + user).then(function () {
		return driver.findElements(webdriver.By.className('card-image-container')).then(function (cards) {
			return cards.length > 10 ? "checkBookshelf: pass" : "checkBookshelf: fail";
		})
	})
}

function checkGroups() {
	return driver.get(url_base + "/group/for/One Down Oy").then(function () {
		return driver.findElements(webdriver.By.className("list-group")).then(function (groups) {
			return groups.length > 3 ? "checkGroups: pass" : "checkGroups: fail";
		})
	})
}

function checkOneGroup() {
	return driver.get(url_base + "/group/5bfbf344e5878400155697d8").then(function () {
		return driver.findElements(webdriver.By.className("activity-wrap")).then(function (acitivities) {
			return acitivities.length > 3 ? "checkOneGroup: pass" : "checkOneGroup: fail";
		})
	})
}

function checkDiscover() {
	return driver.get(url_base + "/discover").then(function () {
		return driver.findElements(webdriver.By.className("card-link")).then(function (groups) {
			return groups.length > 3 ? "checkDiscover: pass" : "checkDiscover: fail";
		})
	})
}


//Logic for running tests as a group
//  funky loop logic for reporting which tests failed despite possible async
//  Add any new tests to the tests array- tests should return a string of "pass"/"fail"
tests = [checkLogin, changeEmail, checkBookshelf, checkGroups, checkOneGroup, checkDiscover];
var t = 0;
for (var test of tests) {
	driver.wait(test, 1000).then(function (passed) {
		console.log("test \t" + passed);
		t += 1;
	}, function (err) {
		console.log("test " + t + " error: " + err);
		t += 1;
	});
}

driver.quit();