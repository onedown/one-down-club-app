# One Down Club App
This is the official One Down Club web app.

### Utilizing:
* Materialize
* jQuery
* VanillaJS


### Dependencies:
* Express
* Cookie parser
* Body parser
* Handlebars
* Flash
* Passport + local strategy
* Session
* Mongo + mongoose
* Cors


## Components

### Codes
- 4 digit code
- tied to a company
- at the moment need to be manually inserted into the db

### Users
- have a company based on code upon registering
- array of groups

### Groups
- owners: array of users who can remove users from the group
- members
- selectBooks: array of book pick objects (book pick = this is the book I will read this month)
- belongs to one company

### Books 
- are based on either manual information or query from bookdepository
- belong to a user


### Remember
Remember to send notifications when something worth of notifying is done


## Content model

### Main
Main
  Main-upper
    h3
  Main-lower
    .content-box
      .content-box-title
      .content-box-content