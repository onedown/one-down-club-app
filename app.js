require('dotenv').config();
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
var expressValidator = require('express-validator');
var flash = require('connect-flash');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongo = require('mongodb');
var mongoose = require('mongoose');
var cors = require('cors');
var sass = require('node-sass');
var i18n = require("i18n");
require('es6-promise').polyfill();
require('isomorphic-fetch');
var handlebars = require('express-handlebars').create(require('./libs/hbsconf.js'));
var hbsHelpers = exphbs.create({
  helpers: require("./helpers/handlebars.js").helpers,
  defaultLayout: 'layout',
  extname: '.handlebars'
});

sass.render({
  file: "./public/css/main.scss",
}, function (err, result) {
  if (err) throw err;
});

process.env.AWS_ACCESS_KEY_ID = process.env.BUCKETEER_AWS_ACCESS_KEY_ID;
process.env.AWS_SECRET_ACCESS_KEY = process.env.BUCKETEER_AWS_SECRET_ACCESS_KEY;
process.env.AWS_REGION = 'eu-west-1';

var AWS = require('aws-sdk');
var s3 = new AWS.S3();

mongoose.connect(process.env.MONGO_URL || "mongodb://localhost:27017/od-v1", {
  useNewUrlParser: true
});
var db = mongoose.connection;

var routes = require('./routes/index');
var users = require('./routes/users');
var bookquery = require('./routes/bookquery');
var bookadd = require('./routes/bookadd');
var bookedit = require('./routes/bookedit');
var userBooks = require('./routes/userBooks');
var settings = require('./routes/settings');
var group = require('./routes/group');
var groupcreate = require('./routes/groupcreate');
var groupjoin = require('./routes/groupjoin');
var userremove = require('./routes/userremove');
var bookdelete = require('./routes/bookdelete');
var adminregister = require('./routes/adminregister');
var manageusers = require('./routes/manageusers');
var deleteuser = require('./routes/deleteuser');
var bookpick = require('./routes/bookpick');
var deletepick = require('./routes/deletepick');
var activity = require('./routes/activity');
var groupleave = require('./routes/groupleave');
var toplist = require('./routes/toplist');
var discover = require('./routes/discover');
var api = require('./routes/api');
var slack = require('./routes/slack');
var company = require('./routes/company');
var bookcomment = require('./routes/bookcomment');
var fileupload = require('./routes/fileupload');
var admins = require('./routes/admins');
var tracks = require('./routes/tracks');
var mods = require('./routes/mods');

// Init App
var app = express();

app.use(cors({
  origin: 'http://localhost:3000'
}));

app.use(express.json());

// View Engine
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

// BodyParser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cookieParser());
app.use(i18n.init);

// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

// Express Session
app.use(session({
  secret: 'secret',
  saveUninitialized: true,
  resave: true
}));

// Passport init
app.use(passport.initialize());
app.use(passport.session());

// Express Validator
app.use(expressValidator({
  errorFormatter: function (param, msg, value) {
    var namespace = param.split('.'),
      root = namespace.shift(),
      formParam = root;

    while (namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param: formParam,
      msg: msg,
      value: value
    };
  }
}));

// Connect Flash
app.use(flash());

// Global Vars
app.use(function (req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  res.locals.user = req.user || null;
  res.locals.bookTitle = req.title;
  res.locals.bookAuthor = req.author;
  next();
});

app.use('/bookadd', bookadd);
app.use('/bookquery', bookquery);
app.use('/', routes);
app.use('/users', users);
app.use('/userBooks', userBooks);
app.use('/bookedit', bookedit);
app.use('/settings', settings);
app.use('/group', group);
app.use('/groupcreate', groupcreate);
app.use('/groupjoin', groupjoin);
app.use('/userremove', userremove);
app.use('/bookdelete', bookdelete);
app.use('/adminregister', adminregister);
app.use('/manageusers', manageusers);
app.use('/deleteuser', deleteuser);
app.use('/bookpick', bookpick);
app.use('/deletepick', deletepick);
app.use('/activity', activity);
app.use('/groupleave', groupleave);
app.use('/toplist', toplist);
app.use('/discover', discover);
app.use('/api', api);
app.use('/slack', slack);
app.use('/company', company);
app.use('/bookcomment', bookcomment);
app.use('/fileupload', fileupload);
app.use('/admins', admins);
app.use('/tracks', tracks);
app.use('/mods', mods);

app.use(function (req, res, next) {
  res.status(404);
  console.log('Req body', req.body);
  // respond with html page
  if (req.accepts('html')) {
    res.render('404', {
      url: req.url
    });
    return;
  }
});

// Set Port
app.set('port', (process.env.PORT || 3000));

app.listen(app.get('port'), function () {
  console.log('Server started on port ' + app.get('port'));
});