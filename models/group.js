var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

// Group Schema
var GroupSchema = mongoose.Schema({
  name: {
    type: String,
  },
  intro: {
    type: String,
  },
  members: {
    type: Array,
    index: true,
  },
  owners: {
    type: Array,
  },
  groupImage: {
    type: String,
  },
  selectBooks: {
    type: Array,
    index: true,
  },
  entries: {
    type: Array,
    index: true,
  },
  company: {
    type: String,
  },
  type: {
    type: String,
  },
}, {
  timestamps: true
});

var groups = module.exports = mongoose.model('groups', GroupSchema);


module.exports.create = (newGroup, callback) => {
  newGroup.save(callback);
}

module.exports.addUser = (groupId, newUserId, callback) => {
  groups.update(groupId, newUserId, callback);
}

module.exports.addBook = (groupId, bookToAdd, callback) => {
  groups.findByIdAndUpdate(
    groupId,
    bookToAdd, {
      new: true,
      upsert: true
    },
    (err, addedBook) => {
      if (err) throw err;
      callback(null, addedBook);
    });
}

module.exports.addActivity = (groupId, newActivity, callback) => {
  groups.findByIdAndUpdate(
    groupId,
    newActivity, {
      new: true
    },
    (err, addedActivity) => {
      if (err) throw err;
      callback(null, addedActivity);
    });
}

module.exports.getByCompany = (company, callback) => {
  var query = {
    company: company,
  };
  groups.find(query, callback);
}

module.exports.getById = (groupId, callback) => {
  groups.findById(groupId, callback);
}

module.exports.getAll = (callback) => {
  groups.find({}, callback);
}

module.exports.destroy = (groupId, callback) => {
  let groupIdUse = {
    _id: groupId
  }
  groups.remove(groupIdUse, (err, deletedGroup) => {
    if (err) throw err;
    callback(null, deletedGroup);
  });
}

module.exports.addBookPick = (groupId, pick, callback) => {
  groups.update(
    groupId,
    pick, {
      new: true,
    },
    (err, addedPick) => {
      if (err) throw err;
      callback(null, addedPick);
    });
}

module.exports.addPic = (groupId, pic, callback) => {
  groups.findByIdAndUpdate(groupId, pic, callback);
}

module.exports.getMemberInGroup = (groupId, memberId, callback) => {
  let query = {
    _id: groupId,
    members: {
      $elemMatch: {
        id: memberId
      }
    }
  }
  groups.find(query, callback);
}

module.exports.deletePicks = (callback) => {
  groups.update({}, {
    members: {
      pick: null
    }
  }, {
    multi: true
  }, callback);
}