var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

// Book Schema
var BookSchema = mongoose.Schema({
  title: {
    type: String,
  },
  author: {
    type: String,
  },
  thoughts: {
    type: String,
  },
  owner: {
    type: String,
  },
  ownerId: {
    type: String,
  },
  bookId: {
    type: String,
  },
  isbn: {
    type: String,
  },
  image: {
    type: String,
  },
  rating: {
    type: Number,
  },
  comments: {
    type: Array,
  },
  date: {
    type: Date,
  },
}, {
  timestamps: true
});

var books = module.exports = mongoose.model('books', BookSchema);

module.exports.createBook = (newBook, callback) => {
  newBook.save(callback);
}

module.exports.addThoughts = (bookId, newThoughts, callback) => {
  books.findByIdAndUpdate(bookId, newThoughts, {
    new: true
  }, (err, books) => {
    if (err) throw err;
    callback(null, books);
  });
}

module.exports.addComment = (bookId, comment, callback) => {
  books.findByIdAndUpdate(bookId, comment, {
    new: true
  }, (err, updatedBook) => {
    if (err) throw err;
    callback(null, updatedBook);
  });
}

module.exports.destroy = (bookId, callback) => {
  let bookIdUse = {
    _id: bookId
  }
  books.remove(bookIdUse, (err, deletedBook) => {
    if (err) throw err;
    callback(null, deletedBook);
  });
}

module.exports.getByUsername = (username, callback) => {
  var query = {
    owner: username,
  };
  books.find(query, callback);
}

module.exports.getByOwnerId = (userId, callback) => {
  var query = {
    ownerId: userId,
  };
  books.find(query, callback);
}

module.exports.getById = (id, callback) => {
  let searchTerm = {
    _id: id,
  };
  books.find(searchTerm, callback);
}