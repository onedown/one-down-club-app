var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

// User Schema
var UserSchema = mongoose.Schema({
	username: {
		type: String,
		index: true
	},
	password: {
		type: String
	},
	email: {
		type: String
	},
	emailSettings: {
		type: Object
	},
	company: {
		type: String
	},
	companyId: {
		type: String
	},
	companyLogo: {
		type: String
	},
	pic: {
		type: String
	},
	groups: {
		type: Array,
		index: true
	},
	admin: {
		type: Boolean
	},
	moderator: {
		type: Boolean
	},
	slackUser: {
		type: String
	},
	slackId: {
		type: String
	}
});

var User = module.exports = mongoose.model('User', UserSchema);

module.exports.create = function (newUser, callback) {
	bcrypt.genSalt(10, function (err, salt) {
		bcrypt.hash(newUser.password, salt, function (err, hash) {
			newUser.password = hash;
			newUser.save(callback);
		});
	});
}

module.exports.setPassword = (userId, newUser, callback) => {
	bcrypt.genSalt(10, function (err, salt) {
		bcrypt.hash(newUser.password, salt, function (err, hash) {
			newUser.password = hash;
			User.update(userId, newUser, callback);
		});
	});
}

module.exports.setEmail = (userId, email, callback) => {
	User.findByIdAndUpdate(userId, email, {
		new: true
	}, (err, newMail) => {
		if (err) throw err;
		callback(null, newMail);
	});
}

module.exports.setPic = (userId, picObj, callback) => {
	User.findByIdAndUpdate(userId, picObj, {
		new: true
	}, (err, newPic) => {
		if (err) throw err;
		callback(null, newPic);
	});
}

module.exports.setSlack = (userId, slackInfo, callback) => {
	User.findByIdAndUpdate(userId, slackInfo, {
		new: true
	}, (err, newSlack) => {
		if (err) throw err;
		callback(null, newSlack);
	});
}

module.exports.addGroup = (userId, groupId, callback) => {
	User.update(userId, groupId, (err, group) => {
		if (err) throw err;
		callback(null, group);
	});
}

module.exports.removeGroup = (userId, groupId, callback) => {
	User.update(userId, groupId, (err, user) => {
		if (err) throw err;
		console.log(user);
	});
}

module.exports.destroy = (id, callback) => {
	let idUse = {
		_id: id
	}
	User.remove(idUse, callback);
}

module.exports.getByEmail = (email, callback) => {
	let query = {
		email: email
	}
	User.find(query, callback);
}

module.exports.getById = function (id, callback) {
	let searchTerm = {
		_id: id,
	};
	User.find(searchTerm, callback);
}

module.exports.getByCompany = function (companyId, callback) {
	let query = {
		companyId: companyId
	}
	User.find(query, callback);
}

// For whatever reason user deserialization seems to want to findById
// so I'll let him have that
// EDIT: silly me, obviously findById returns only one result 
// and find returns an array of results
module.exports.getByIdForSerialization = function (id, callback) {
	User.findById(id, callback);
}

module.exports.getByUsername = function (username, callback) {
	var query = {
		username: username
	};
	User.findOne(query, callback);
}

module.exports.getBySlackId = function (slackID, callback) {
	var query = {
		slackId: slackID
	};
	User.findOne(query, callback);
}

module.exports.getBySlackUser = function (slackUser, callback) {
	var query = {
		slackUser: slackUser
	};
	User.findOne(query, callback);
}

module.exports.getAll = (callback) => {
	let queryTerm = {

	}
	User.find(queryTerm, callback);
}

module.exports.comparePassword = function (candidatePassword, hash, callback) {
	bcrypt.compare(candidatePassword, hash, function (err, isMatch) {
		if (err) throw err;
		callback(null, isMatch);
	});
}

module.exports.setPermissions = (userid, perms, callback) => {
	User.update(userid, perms, callback);
}