var mongoose = require('mongoose');

var RecCategorySchema = mongoose.Schema({
  category: {
    type: String,
  },
  description: {
    type: String,
  },
}, {
  timestamps: true
});

var reccats = module.exports = mongoose.model('reccats', RecCategorySchema);

module.exports.createRec = (newReccat, callback) => {
  newReccat.save(callback);
}

module.exports.getAll = (callback) => {
  reccats.find({}, callback);
}

module.exports.editRec = (recId, newContent, callback) => {
  reccats.findByIdAndUpdate(recId, newContent, callback);
}