var mongoose = require('mongoose');

// Toplist Schema
var ToplistSchema = mongoose.Schema({
  company: {
    type: String,
  },
  companyId: {
    type: String,
  },
  users: {
    type: Object,
  },
}, {
  timestamps: true
});

var toplist = module.exports = mongoose.model('toplist', ToplistSchema);

module.exports.create = (newToplist, callback) => {
  newToplist.save(callback);
}

module.exports.updateUser = (userId, user, callback) => {
  toplist.findByIdAndUpdate(userId, user, {
    new: true
  }, (err, result) => {
    if (err) throw err;
    callback(null, result)
  });
}