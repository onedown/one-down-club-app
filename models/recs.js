var mongoose = require('mongoose');

// Book Schema
var RecSchema = mongoose.Schema({
  title: {
    type: String,
  },
  author: {
    type: String,
  },
  index: {
    type: Number,
  },
  desc: {
    type: String,
  },
  category: {
    type: String,
  },
  image: {
    type: String,
  },
  link: {
    type: String,
  },
}, {
  timestamps: true
});

var recs = module.exports = mongoose.model('recs', RecSchema);

module.exports.createRec = (newRec, callback) => {
  newRec.save(callback);
}

module.exports.editIndex = (recId, newIndex, callback) => {
  recs.findByIdAndUpdate(recId, newIndex, {
    new: true
  }, (err, rec) => {
    if (err) throw err;
    callback(null, rec);
  });
}

module.exports.getAll = (callback) => {
  recs.find({}, callback);
}

module.exports.getByCategory = (term, callback) => {
  let query = {
    category: term
  }
  recs.find(query, callback);
}

module.exports.destroy = (id, callback) => {
  idUse = {
    _id: id
  }
  recs.remove(idUse, callback)
}