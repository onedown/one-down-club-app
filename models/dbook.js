var mongoose = require('mongoose');

// Book Schema
var DBookSchema = mongoose.Schema({
  title: {
    type: String,
  },
  author: {
    type: String,
  },
  company: {
    type: String,
  },
  isbn: {
    type: String,
  },
  image: {
    type: String,
  },
  avgRating: {
    type: Number,
  },
  reviews: {
    type: Array,
  },
  bookId: {
    type: String,
  },
}, {
  timestamps: true
});

var dbooks = module.exports = mongoose.model('dbooks', DBookSchema);

module.exports.createDBook = (newBook, callback) => {
  newBook.save(callback);
}

module.exports.getByIsbn = (isbnToSearch, callback) => {
  var query = {
    isbn: isbnToSearch,
  }
  dbooks.find(query, callback);
}

module.exports.getById = (idToSearch, callback) => {
  dbooks.findById(idToSearch, callback);
}

module.exports.getByIsbnCompany = (query, callback) => {
  dbooks.find(query, (err, results) => {
    if (err) throw err;
    callback(null, results);
  });
}

module.exports.addDBook = (query, newBook, callback) => {
  dbooks.update(query, newBook, (err, book) => {
    if (err) throw err;
    callback(null, book);
  });
}

module.exports.getByCompany = (query, callback) => {
  let searchTerm = {
    company: query,
  }
  dbooks.find(searchTerm, callback);
}

module.exports.destroyByBookId = (bookId, callback) => {
  let bookIdUse = {
    bookId: bookId
  }
  dbooks.remove(bookIdUse, callback);
}

module.exports.getByBookId = (bookId, callback) => {
  let bookIdUse = {
    bookId: bookId
  }
  dbooks.find(bookIdUse, callback);
}


module.exports.getByName = (name, callback) => {
  let term = {
    $or: [{
      title: {
        "$regex": name,
        "$options": "i"
      },
    }, {
      author: {
        "$regex": name,
        "$options": "i"
      }
    }],
  }
  dbooks.find(term, callback);
}