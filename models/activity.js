var mongoose = require('mongoose');

// Activity Schema
var ActivitySchema = mongoose.Schema({
  user: {
    type: String,
  },
  userId: {
    type: String,
  },
  company: {
    type: String,
  },
  companyId: {
    type: String,
  },
  type: {
    type: String,
  },
  groupId: {
    type: String,
  },
  link: {
    type: String,
  },
  data: {
    type: Object,
  },
}, {
  timestamps: true
});

var activity = module.exports = mongoose.model('activity', ActivitySchema);

module.exports.createActivity = (activity, callback) => {
  activity.save(callback);
}

module.exports.getByCompany = (companyId, callback) => {
  let searchTerm = {
    companyId: companyId,
  }
  activity.find(searchTerm, callback);
}

module.exports.getByGroup = (groupIdUse, callback) => {
  let searchTerm = {
    groupId: groupIdUse,
  };
  activity.find(searchTerm, callback);
}

module.exports.destroyByBookId = (bookId, callback) => {
  activity.remove(bookId, (err, deleted) => {
    if (err) throw err;
    console.log(deleted);
  });
}