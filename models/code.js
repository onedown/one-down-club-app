var mongoose = require('mongoose');

// Code Schema
var CodeSchema = mongoose.Schema({
  accessCode: {
    type: String,
    index: true,
  },
  company: {
    type: String,
    index: true,
  },
  toplist: {
    type: Object,
  },
  logo: {
    type: String,
  },
  cover: {
    type: String,
  },
  color: {
    type: String,
  },
}, {
  timestamps: true
});

var codes = module.exports = mongoose.model('codes', CodeSchema);

module.exports.createCompany = (newCompany, callback) => {
  newCompany.save(callback);
}

module.exports.getByCompany = (company, callback) => {
  let query = {
    company: company,
  }
  codes.find(query, callback);
}

module.exports.addToToplist = (companyId, user, callback) => {
  codes.findByIdAndUpdate(
    companyId, user, {
      new: true
    }, (err, user) => {
      if (err) throw err;
      callback(null, user);
    });
}

module.exports.setLogo = (companyId, newLogo, callback) => {
  codes.findByIdAndUpdate(companyId, newLogo, {
    new: true
  }, (err, updatedCompany) => {
    if (err) throw err;
    callback(null, updatedCompany);
  });
}

module.exports.setAttribute = (companyId, newAttr, callback) => {
  codes.findByIdAndUpdate(companyId, newAttr, {
    new: true
  }, (err, updatedCompany) => {
    if (err) throw err;
    callback(null, updatedCompany);
  });
}

module.exports.getByCompanyId = (companyId, callback) => {
  codes.findById(companyId, callback);
}

module.exports.getByCode = (code, callback) => {
  let query = {
    accessCode: {
      $regex: code
    }
  }
  codes.findOne(query, (err, results) => {
    if (err) throw err;
    callback(null, results);
  });
}

module.exports.getAll = (callback) => {
  codes.find({}, callback);
}