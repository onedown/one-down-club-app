// Check if db is empty and insert a default user and default company
users.getAll((err, result) => {
  if (result.length < 1) {
    let defaultCompany = new accessCode({
      accessCode: "ODC69",
      company: "One Down Oy",
      logo: "https://onedown.club/wp-content/uploads/2018/05/OneDown_ilman_taustaa_valk_teksti.png",
      cover: "https://onedown.club/wp-content/uploads/2018/05/OneDown_ilman_taustaa_valk_teksti.png",
      color: "#698FF6",
    });
    accessCode.createCompany(defaultCompany, (err, newCompany) => {
      if (err) throw err;
      let defaultUser = new users({
        username: "admin",
        password: "123",
        email: "latezio@gmail.com",
        company: newCompany.company,
        companyId: newCompany._id,
        companyLogo: "https://onedown.club/wp-content/uploads/2018/05/OneDown_ilman_taustaa_valk_teksti.png",
        admin: true,
        moderator: true,
        pic: "http://www.viikonloppu.com/wp-content/uploads/2014/04/slide_245969_1407896_free-619x464.jpg",
        emailSettings: {
          onGroupPost: true,
          onGroupPick: false,
          onNewComment: true,
        }
      });
      users.create(defaultUser, (err, created) => {
        if (err) throw err;
        console.log(created);
      });
    });
  }
});